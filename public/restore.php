<?php
require_once dirname(__DIR__) . '/conf/conf.php';

require_once dirname(__FILE__) . '/application-top.php';

function recursiveDelete($str) {
    if (is_file($str)) {
        return @unlink($str);
    }
    elseif (is_dir($str)) {
        $scan = glob(rtrim($str,'/').'/*');
        foreach($scan as $index=>$path) {
            recursiveDelete($path);
        }
        return @rmdir($str);
    }
}

function full_copy( $source, $target,$empty_first=true) {
	if ($empty_first){	
		recursiveDelete($target);
	}
	
    if ( is_dir( $source ) ) {
        @mkdir( $target );
        $d = dir( $source );
        while ( FALSE !== ( $entry = $d->read() ) ) {
            if ( $entry == '.' || $entry == '..' ) {
                continue;
            }
            $Entry = $source . '/' . $entry; 
            if ( is_dir( $Entry ) ) {
                full_copy( $Entry, $target . '/' . $entry );
                continue;
            }
            copy( $Entry, $target . '/' . $entry );
        }

        $d->close();
    }else {
        copy( $source, $target );
    }
}

function restoreDatabase($backupFile,$concate_path=true) {
	$db_server = CONF_DB_SERVER;
	$db_user = CONF_DB_USER;
	$db_password = CONF_DB_PASS;
	$db_databasename = CONF_DB_NAME;
			
	$sql = "SHOW TABLES FROM $db_databasename";
	if($rs = FatApp::getDb()->query($sql)){
		  while($row = FatApp::getDb()->fetch($rs)){
				$table_name=$row["Tables_in_".$db_databasename];
				FatApp::getDb()->query("DROP TABLE $db_databasename.$table_name");
		  }
	}
	$cmd ="mysql --user=" . $db_user . " --password='" . $db_password . "' " . $db_databasename . " < " . $backupFile;
	system($cmd);
	
}

try{ 
	if(!isset($_GET['passkey']) || $_GET['passkey']!='efalak-restore') {		
		throw new Exception('Access denied!!');
	}
	
	/* $source = CONF_INSTALLATION_PATH."restore/user-uploads";
	$target = CONF_UPLOADS_PATH;
	full_copy($source,$target);
	echo "Uploads folder restored.<br/>";  */
	
	/* $file = CONF_INSTALLATION_PATH."restore/database/fatbitco_efalak.sql";
	restoreDatabase($file,false);
	echo "Database restored.<br/>"; */
	
	
}catch(Exception $e) {
   echo $e->getMessage();
}