$(document).ready(function(){
	searchProducts(document.frmSearch);
});
(function() {
	var currentProdId = 0;	
	var currentPage = 1;	
	var runningAjaxReq = false;
	var dv = '#listing';
	searchProducts = function(frm){
		if( runningAjaxReq == true ){
			return;
		}
		runningAjaxReq = true;
		/*[ this block should be before dv.html('... anything here.....') otherwise it will through exception in ie due to form being removed from div 'dv' while putting html*/
		var data = '';
		if (frm) {
			data = fcom.frmData(frm);
		}
		/*]*/
		var dv = $('#listing');
		$(dv).html( fcom.getLoader() );
		
		fcom.ajax(fcom.makeUrl('SellerProducts','sellerProducts'),data,function(res){
			runningAjaxReq = false;
			$("#listing").html(res);
		});
	};
	addSellerProductForm = function(product_id, selprod_id){
		/* if( !product_id && selprod_id == 0 ){
			return;
		} */
		$.facebox(function() {	sellerProductForm(product_id, selprod_id); });
	};
	
	sellerProductForm = function(product_id, selprod_id){
		/* if( !product_id && selprod_id == 0 ){
			return;
		} */
		fcom.displayProcessing();
			fcom.ajax(fcom.makeUrl('SellerProducts', 'sellerProductForm', [ product_id, selprod_id]), '', function(t) {
				fcom.updateFaceboxContent(t);
			});
	};
	
	setUpSellerProduct = function(frm){
		if( runningAjaxReq == true ){
			console.log(runningAjaxMsg);
			return;
		}
		var data = fcom.frmData(frm);
		runningAjaxReq = true;
		var data = fcom.frmData(frm);
		fcom.updateWithAjax(fcom.makeUrl('SellerProducts', 'setUpSellerProduct'), data, function(t) {
			runningAjaxReq = false;
			if(t.selprod_id > 0){
				$(frm.splprice_selprod_id).val(t.selprod_id);
			}
			if(t.langId > 0){
				sellerProductLangForm(t.selprod_id,t.langId);				
			}			
		});
	};
		
	sellerProductLangForm = function( selprod_id, lang_id ){
		fcom.resetEditorInstance();
		fcom.displayProcessing();
			fcom.ajax(fcom.makeUrl('SellerProducts', 'sellerProductLangForm', [ selprod_id, lang_id ]), '', function(t) {
				fcom.updateFaceboxContent(t);
				fcom.setEditorLayout(lang_id);
			});
	};
	
	sellerProductDelete=function(id){
		if(!confirm(langLbl.confirmDelete)){return;}
		data='id='+id;
		fcom.updateWithAjax(fcom.makeUrl('SellerProducts','sellerProductDelete'),data,function(res){		
			reloadList();
		});
	};
	
	setUpSellerProductLang = function(frm){
		if (!$(frm).validate()) return;
		if( runningAjaxReq == true ){
			console.log(runningAjaxMsg);
			return;
		}
		runningAjaxReq = true;
		var data = fcom.frmData(frm);
		fcom.updateWithAjax(fcom.makeUrl('SellerProducts', 'setUpSellerProductLang'), data, function(t) {
			runningAjaxReq = false;			
			if(t.selprod_id > 0){
				$(frm.splprice_selprod_id).val(t.selprod_id);
			}
			if(t.langId > 0){
				sellerProductLangForm(t.selprod_id,t.langId);				
			}			
		});
	};
	addSellerProductSpecialPrices = function( selprod_id ){
		$.facebox(function() { sellerProductSpecialPrices(selprod_id);});
	};
	
	
	sellerProductSpecialPrices = function( selprod_id ){
		fcom.displayProcessing();
			fcom.ajax(fcom.makeUrl('SellerProducts', 'sellerProductSpecialPrices', [ selprod_id ]), '', function(t) {
				fcom.updateFaceboxContent(t);
			});
		
	};
	
	sellerProductSpecialPriceForm = function( selprod_id, splprice_id ){
		if(typeof splprice_id==undefined || splprice_id == null){
			splprice_id = 0;
		}
		$.facebox(function() {
			fcom.ajax(fcom.makeUrl('SellerProducts', 'sellerProductSpecialPriceForm', [selprod_id, splprice_id ]), '', function(t) {
				$.facebox(t,'faceboxWidth');
			});
		});
	};
	
	setUpSellerProductSpecialPrice = function(frm){
		if (!$(frm).validate()) return;
		if( runningAjaxReq == true ){
			console.log(runningAjaxMsg);
			return;
		}
		runningAjaxReq = true;
		var data = fcom.frmData(frm);
		fcom.updateWithAjax(fcom.makeUrl('SellerProducts', 'setUpSellerProductSpecialPrice'), data, function(t) {
			runningAjaxReq = false;
			sellerProductSpecialPrices( $(frm.splprice_selprod_id).val() );
			$(document).trigger('close.facebox');
		});
		return false;
	};
		
	deleteSellerProductSpecialPrice = function( splprice_id ){
		var agree = confirm(langLbl.confirmDelete);
		if( !agree ){ return false; }
		fcom.updateWithAjax(fcom.makeUrl('SellerProducts', 'deleteSellerProductSpecialPrice'), 'splprice_id=' + splprice_id, function(t) {
			sellerProductSpecialPrices( t.selprod_id );
			$(document).trigger('close.facebox');
		});
	};
	
	sellerProductVolumeDiscounts = function( selprod_id ){
		fcom.displayProcessing();
			fcom.ajax(fcom.makeUrl('SellerProducts', 'sellerProductVolumeDiscounts', [ selprod_id ]), '', function(t) {
				fcom.updateFaceboxContent(t);
			});
	};
	
	sellerProductVolumeDiscountForm = function( selprod_id, voldiscount_id ){
		if( typeof voldiscount_id == undefined || voldiscount_id == null ){
			voldiscount_id = 0;
		}
		$.facebox(function() {
			fcom.displayProcessing();
			fcom.ajax(fcom.makeUrl('SellerProducts', 'sellerProductVolumeDiscountForm', [selprod_id, voldiscount_id ]), '', function(t) {
				fcom.updateFaceboxContent(t);
			});
		});
	};
	
	setUpSellerProductVolumeDiscount = function( frm ){
		if (!$(frm).validate()) return;
		if( runningAjaxReq == true ){
			console.log(runningAjaxMsg);
			return;
		}
		runningAjaxReq = true;
		var data = fcom.frmData(frm);
		fcom.updateWithAjax(fcom.makeUrl('SellerProducts', 'setUpSellerProductVolumeDiscount'), data, function(t) {
			runningAjaxReq = false;
			sellerProductVolumeDiscounts( $(frm.voldiscount_selprod_id).val() );
			$(document).trigger('close.facebox');
		});
		return false;
	};
	
	deleteSellerProductVolumeDiscount = function( voldiscount_id ){
		var agree = confirm(langLbl.confirmDelete);
		if( !agree ){ return false; }
		fcom.updateWithAjax(fcom.makeUrl('SellerProducts', 'deleteSellerProductVolumeDiscount'), 'voldiscount_id=' + voldiscount_id, function(t) {
			sellerProductVolumeDiscounts( t.selprod_id );
			$(document).trigger('close.facebox');
		});
	}
	
	cancelForm = function(frm){
		searchProducts(document.frmSearch);
		$(document).trigger('close.facebox');
	};
	
	productSeo = function (selprod_id){
		fcom.displayProcessing();
			fcom.ajax(fcom.makeUrl('SellerProducts', 'productSeo', [ selprod_id ]), '', function(t) {
				fcom.updateFaceboxContent(t);
				getProductSeoGeneralForm(selprod_id);	
			});
	};
	
	getProductSeoGeneralForm = function (selprod_id){
				fcom.displayProcessing();

			fcom.ajax(fcom.makeUrl('SellerProducts', 'productSeoGeneralForm'), 'selprod_id='+selprod_id, function(t) {
				fcom.updateFaceboxContent(t);
			});
		
	}
	
	setupProductMetaTag = function (frm){
		if (!$(frm).validate()) return;		
		var data = fcom.frmData(frm);
		fcom.updateWithAjax(fcom.makeUrl('SellerProducts', 'setupProdMeta'), data, function(t) {
			
			if (t.langId>0) {
				editProductMetaTagLangForm(t.metaId, t.langId, t.metaType);
				return ;
			}
		
		});
	}
	
	setupProductLangMetaTag = function (frm){
		if (!$(frm).validate()) return;		
		var data = fcom.frmData(frm);
		fcom.updateWithAjax(fcom.makeUrl('SellerProducts', 'setupProdMetaLang'), data, function(t) {
			
			if (t.langId>0) {
				editProductMetaTagLangForm(t.metaId, t.langId, t.metaType);
				return ;
			}
		


		});

	}
	
	editProductMetaTagLangForm = function(metaId,langId, metaType){
		fcom.displayProcessing();
			fcom.ajax(fcom.makeUrl('SellerProducts', 'productSeoLangForm', [metaId,langId,metaType]), '', function(t) {
				fcom.updateFaceboxContent(t);
			});
	};
	
	sellerProductLinkFrm = function( selprod_id ) {
		fcom.displayProcessing();
		fcom.ajax(fcom.makeUrl('SellerProducts', 'sellerProductLinkFrm', [ selprod_id ]), '', function(t) {
			fcom.updateFaceboxContent(t);
		});
	}
	
	setUpSellerProductLinks = function(frm){
		if (!$(frm).validate()) return;
		var data = fcom.frmData(frm);
		fcom.updateWithAjax(fcom.makeUrl('SellerProducts', 'setupSellerProductLinks'), data, function(t) {
		});
	}
	
	sellerProductDownloadFrm = function( selprod_id ) {
		fcom.displayProcessing();
		fcom.ajax(fcom.makeUrl('SellerProducts', 'sellerProductDownloadFrm', [ selprod_id ]), '', function(t) {
			fcom.updateFaceboxContent(t);
		});	
	};

	setUpSellerProductDownloads = function ( ){
		selprod_id = $('#frmDownload input[name=selprod_id]').val();
		var data = new FormData(  );
		$inputs = $('#frmDownload input[type=text],#frmDownload select,#frmDownload input[type=hidden]');
		$inputs.each(function() { data.append( this.name,$(this).val());});	
		
		$.each( $('#downloadable_file')[0].files, function(i, file) {
			$(dv).html(fcom.getLoader());
			data.append('downloadable_file', file);
			$.ajax({
				url : fcom.makeUrl('SellerProducts', 'uploadDigitalFile'),
				type: "POST",
				data : data,
				processData: false,
				contentType: false,
				success: function(t){
					var ans = $.parseJSON(t);
					$.systemMessage( ans.msg );
					sellerProductDownloadFrm(selprod_id);					
				},
				error: function(jqXHR, textStatus, errorThrown){
					alert("Error Occured.");
				}
			});
		});	
	};
	
	deleteDigitalFile = function(selprod_id,afile_id){
		var agree = confirm(langLbl.confirmDelete);
		if( !agree ){ return false; }
		fcom.ajax( fcom.makeUrl( 'SellerProducts', 'deleteDigitalFile', [selprod_id, afile_id] ), '' , function(t) {
			var ans = $.parseJSON(t);
			if( ans.status == 1 ){			
				fcom.displaySuccessMessage(ans.msg);
			} else {
				fcom.displayErrorMessage(ans.msg);
			}
			sellerProductDownloadFrm( selprod_id );
		});
	};	
	
	linkPoliciesForm = function(product_id , selprod_id,ppoint_type){
			fcom.displayProcessing();
			fcom.ajax(fcom.makeUrl('SellerProducts', 'linkPoliciesForm', [ product_id, selprod_id, ppoint_type]), '', function(t) {
				fcom.updateFaceboxContent(t);
				searchPoliciesToLink();
						

			});

	};
	
	searchPoliciesToLink = function(form){
		var form = (form) ? form : document.frmLinkWarrantyPolicies;
		var data = '';
		if (form) {
			data = fcom.frmData(form);
		}		
		
		fcom.ajax(fcom.makeUrl('SellerProducts','searchPoliciesToLink'),data,function(res){
			$('#listPolicies').html(res);	fcom.resetFaceboxHeight();
		});
	};
	
	addPolicyPoint = function(selprod_id,ppoint_id){
		var data='selprod_id='+selprod_id+'&ppoint_id='+ppoint_id;
		
		fcom.ajax(fcom.makeUrl('SellerProducts','addPolicyPoint'),data,function(res){
			searchPoliciesToLink();
		});
	};
	
	removePolicyPoint = function(selprod_id,ppoint_id){
		var data='selprod_id='+selprod_id+'&ppoint_id='+ppoint_id;
		fcom.ajax(fcom.makeUrl('SellerProducts','removePolicyPoint'),data,function(res){
			searchPoliciesToLink();
		});
	};
	
	goToNextPolicyToLinkPage = function(page) {	
		if(typeof page==undefined || page == null){
			page =1;
		}
		var frm = document.frmPolicyToLinkSearchPaging;		
		$(frm.page).val(page);
		searchPoliciesToLink(frm);
	};
	
	
	
	
	
	goToSearchPage = function(page) {
		if(typeof page==undefined || page == null){
			page =1;
		}		
		var frm = document.frmProductSearchPaging;		
		$(frm.page).val(page);
		searchProducts(frm);
	}

	reloadList = function() {
		var frm = document.frmSearch;
		searchProducts(frm);
	}

	
	productAttributeGroupForm = function( ){
		$.facebox(function() {
			fcom.ajax(fcom.makeUrl('SellerProducts', 'productAttributeGroupForm'), '', function(t) {
				$.facebox(t,'faceboxWidth');
			});
		});
	}

	
	setupProduct = function(frm) {
		if (!$(frm).validate()) return;
		var addingNew = ($(frm.product_id).val() == 0);
		var data = fcom.frmData(frm);
		fcom.updateWithAjax(fcom.makeUrl('SellerProducts', 'setUpSellerProduct'), data, function(t) {
			reloadList();
			if (addingNew) {
				productLangForm(t.product_id, t.lang_id);
				return ;
			}
			$(document).trigger('close.facebox');
		});
	};
	
	setupProductLang = function(frm){
		if ( !$(frm).validate() ) return;
		var data = fcom.frmData(frm);		
		fcom.updateWithAjax(fcom.makeUrl('SellerProducts', 'setUpSellerProductLang'), data, function(t) {
			reloadList();				
			if (t.lang_id>0) {
				productLangForm(t.product_id, t.lang_id);
				return ;
			}
			$(document).trigger('close.facebox');
			return ;
		});
		return;
	};
	
	clearSearch = function(){
		document.frmSearch.reset();
		searchProducts(document.frmSearch);
	};
	
	toggleStatus = function(e,obj){
		if(!confirm(langLbl.confirmUpdateStatus)){
			e.preventDefault();
			return;
		}
		var selprodId = parseInt(obj.value);
		if( selprodId < 1 ){
			fcom.displayErrorMessage(langLbl.invalidRequest);
			return false;
		}
		data='selprodId='+selprodId;
		fcom.ajax(fcom.makeUrl('SellerProducts','changeStatus'),data,function(res){
		var ans = $.parseJSON(res);
			if( ans.status == 1 ){
				$(obj).toggleClass("active");
				fcom.displaySuccessMessage(ans.msg);
			} else {
				fcom.displayErrorMessage(ans.msg);
			}
		});
	};
	
})();


