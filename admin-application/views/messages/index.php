<?php defined('SYSTEM_INIT') or die('Invalid Usage.');?>

<div class='page'>
	<div class='fixed_container'>
		<div class="row">
			<div class="space">
				<div class="page__title">
					<div class="row">
						<div class="col--first col-lg-6">
							<span class="page__icon">
							<i class="ion-android-star"></i></span>
							<h5><?php echo Labels::getLabel('LBL_Messages',$adminLangId); ?> </h5>
							<?php $this->includeTemplate('_partial/header/header-breadcrumb.php'); ?>
						</div>
					</div>
				</div>
				<section class="section searchform_filter">
					<div class="sectionhead">
						<h4> <?php echo Labels::getLabel('LBL_Search...',$adminLangId); ?></h4>
					</div>
					<div class="sectionbody space togglewrap" style="display:none;">
						<?php 
							$search->setFormTagAttribute ( 'onsubmit', 'searchMessages(this); return(false);');
							$search->setFormTagAttribute ( 'class', 'web_form' );
							$search->developerTags['colClassPrefix'] = 'col-md-';
							$search->developerTags['fld_default_col'] = 4;
							$btn_clear = $search->getField('btn_clear');
							$btn_clear->addFieldTagAttribute('onclick', 'clearSearch()');
							echo $search->getFormHtml();
						?>
					</div>
				</section>
				<div id="listing">
					<?php echo Labels::getLabel('LBL_Processing...',$adminLangId); ?>
				</div>

			</div>
		</div>
	</div>
</div>