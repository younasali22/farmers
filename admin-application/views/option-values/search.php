<?php defined('SYSTEM_INIT') or die('Invalid Usage.'); ?>
<?php
$arr_flds = array(
		'listserial'=>Labels::getLabel('LBL_SR_NO',$adminLangId),
		'optionvalue_identifier'=>Labels::getLabel('LBL_OPTION_VALUE_NAME',$adminLangId),			
		'action' => Labels::getLabel('LBL_ACTION',$adminLangId),
	);
$tbl = new HtmlElement('table', array('width'=>'100%', 'class'=>'table table-responsive table--hovered'));
$th = $tbl->appendElement('thead')->appendElement('tr');
foreach ($arr_flds as $val) {
	$e = $th->appendElement('th', array(), $val);
}

$sr_no = 0;
foreach ($arr_listing as $sn=>$row){
	$sr_no++;
	$tr = $tbl->appendElement('tr');
	$tr->setAttribute ("id",$row['optionvalue_id']);	
	foreach ($arr_flds as $key=>$val){
		$td = $tr->appendElement('td');
		switch ($key){
			case 'listserial':
				$td->appendElement('plaintext', array(), $sr_no);
			break;
			case 'optionvalue_identifier':
				if($row['optionvalue_name']!=''){
					$td->appendElement('plaintext', array(), $row['optionvalue_name']);
					$td->appendElement('br', array());
					$td->appendElement('plaintext', array(), '('.$row[$key].')');
				}else{
					$td->appendElement('plaintext', array(), $row[$key]);
				}
				break;						
			case 'action':
				$ul = $td->appendElement("ul",array("class"=>"actions actions--centered"));
				if($canEdit){
					$li = $ul->appendElement("li",array('class'=>'droplink'));
					$li->appendElement('a', array('href'=>'javascript:void(0)', 'class'=>'button small green','title'=>Labels::getLabel('LBL_Edit',$adminLangId)),'<i class="ion-android-more-horizontal icon"></i>', true);
              		$innerDiv=$li->appendElement('div',array('class'=>'dropwrap'));
              		$innerUl=$innerDiv->appendElement('ul',array('class'=>'linksvertical'));
              		$innerLiEdit=$innerUl->appendElement('li');
					$innerLiEdit->appendElement('a', array('href'=>'javascript:void(0)', 
					'class'=>'button small green', 'title'=>Labels::getLabel('LBL_Edit',$adminLangId),
					"onclick"=>"optionValueForm(".$row['optionvalue_option_id'].",".$row['optionvalue_id'].")"),Labels::getLabel('LBL_Edit',$adminLangId), true);

					$innerLiDelete = $innerUl->appendElement("li");
					$innerLiDelete->appendElement('a', array('href'=>"javascript:void(0)", 
					'class'=>'button small green', 'title'=>Labels::getLabel('LBL_Delete',$adminLangId),"onclick"=>"deleteOptionValue(".$row['optionvalue_option_id'].",".$row['optionvalue_id'].")"),Labels::getLabel('LBL_Delete',$adminLangId), true);
				}
			break;
			default:
				$td->appendElement('plaintext', array(), $row[$key]);
			break;
		}
	}
}
if (count($arr_listing) == 0){
	$tbl->appendElement('tr')->appendElement('td', array('colspan'=>count($arr_flds)), 
	Labels::getLabel('MSG_NO_RECORD_FOUND',$adminLangId));
}
echo $tbl->getHtml();
?>