<?php defined('SYSTEM_INIT') or die('Invalid Usage.');?>

    <section class="section">
        <div class="sectionhead">
            <h4><?php echo Labels::getLabel('LBL_Shop_Setup',$adminLangId); ?></h4>
        </div>
        <div class="sectionbody space">
          
			<div class=" tabs_nav_container  flat">
				<ul class="tabs_nav">
					<li>
						<a href="javascript:void(0)" onclick="shopForm(<?php echo $shop_id ?>);">
							<?php echo Labels::getLabel('LBL_General',$adminLangId); ?>
						</a>
					</li>
					<?php 
					$inactive=($shop_id==0)?'fat-inactive':'';	
					foreach($languages as $languageId=>$langName){?>
					<li class="<?php echo $inactive;?>"><a href="javascript:void(0);" <?php if($shop_id>0){?> onclick="addShopLangForm(<?php echo $shop_id ?>, <?php echo $languageId;?>);" <?php }?>><?php echo Labels::getLabel('LBL_'.$langName,$adminLangId);;?></a></li>
					<?php } ?>
					<li><a href="javascript:void(0);" <?php if($shop_id>0){?> onclick="shopTemplates(<?php echo $shop_id ?>);" <?php }?>><?php echo Labels::getLabel('LBL_Templates',$adminLangId); ?></a></li>
					<li><a href="javascript:void(0);" <?php if($shop_id>0){?> onclick="shopMediaForm(<?php echo $shop_id ?>);" <?php }?>><?php echo Labels::getLabel('LBL_Media',$adminLangId); ?></a></li>

					<li><a class="active" href="javascript:void(0);" <?php if($shop_id>0){?> onclick="shopCollectionProducts(<?php echo $shop_id ?>);" <?php }?>><?php echo Labels::getLabel('LBL_Collection',$adminLangId); ?></a></li>
				</ul>
				<div class="tabs_panel_wrap">
					<ul class="tabs_nav tabs_nav--internal">
						<li>
							<a onclick="getShopCollectionGeneralForm(<?php echo $shop_id; ?>);" href="javascript:void(0)">
								<?php echo Labels::getLabel('TXT_GENERAL', $adminLangId);?>
							</a>
						</li>
					<?php 					
						foreach($language as $lang_id => $langName){?>
						<li>
							<a class="<?php echo ($langId == $lang_id)?'active':''?>" href="javascript:void(0)" onClick="editShopCollectionLangForm(<?php echo $shop_id;?>, <?php echo $scollection_id ?>, <?php echo $lang_id;?>)">
								<?php echo Labels::getLabel('LBL_'.$langName,$adminLangId);?>
							</a>
						</li>
						<?php } ?>
						<li>
							<a onclick="sellerCollectionProducts(<?php echo $scollection_id ?>,<?php echo $shop_id; ?>)" href="javascript:void(0);">
								<?php echo Labels::getLabel('TXT_LINK', $adminLangId);?>
							</a>
						</li>
					</ul>
					<div class="tabs_panel_wrap">
						<div class="form__subcontent">
							<?php 
								$shopColLangFrm->setFormTagAttribute('class', 'form form_horizontal web_form layout--'.$formLayout);
								$shopColLangFrm->setFormTagAttribute('onsubmit', 'setupShopCollectionlangForm(this); return(false);');
								 $shopColLangFrm->developerTags['colClassPrefix'] = 'col-md-';
								$shopColLangFrm->developerTags['fld_default_col'] = 12;
								echo $shopColLangFrm->getFormHtml();
								?>
						</div>
					</div>
				</div>
			</div>
               
        </div>
    </section>