<?php defined('SYSTEM_INIT') or die('Invalid Usage.');?>
<section class="section">
<div class="sectionhead">
	<h4><?php echo Labels::getLabel('LBL_Collections_Layout_Instructions',$adminLangId); ?></h4>
</div>
<div class="sectionbody space">
	<div class="row">
		<div class="col-sm-12"> 
			<h1><?php //echo Labels::getLabel('LBL_Collections_Layout_Instructions',$adminLangId);?></h1>
		</div>
		<div class="col-sm-12">
			<div class="row"> 		
				<section class="section">
					<div class="sectionbody">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<div class="shop-template fixed-height-div"> 
								<a rel="facebox" onClick="displayImageInFacebox('<?php echo FatUtility::generateFullUrl(); ?><?php echo CONF_WEBROOT_FRONT_URL; ?>images/collection_layouts/layout-1.jpg');" href="javascript:void(0)">
									<figure class="thumb--square"><img width="400px;" src="<?php echo CONF_WEBROOT_URL; ?>images/collection_layouts/layout-1.jpg" /></figure>
									<p><?php echo Labels::getLabel('LBL_Layout_1',$adminLangId);?></p>
								</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<div class="shop-template fixed-height-div">
								<a rel="facebox" onClick="displayImageInFacebox('<?php echo FatUtility::generateFullUrl(); ?><?php echo CONF_WEBROOT_FRONT_URL; ?>images/collection_layouts/layout-2.jpg');" href="javascript:void(0)">
									<figure class="thumb--square"><img width="400px;" src="<?php echo CONF_WEBROOT_URL; ?>images/collection_layouts/layout-2.jpg" /></figure>
									<p><?php echo Labels::getLabel('LBL_Layout_2',$adminLangId);?></p>
								</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="min-height:500px">
							<div class="shop-template fixed-height-div">
								<a rel="facebox" onClick="displayImageInFacebox('<?php echo FatUtility::generateFullUrl(); ?><?php echo CONF_WEBROOT_FRONT_URL; ?>images/collection_layouts/layout-3.jpg');" href="javascript:void(0)">
									<figure class="thumb--square"><img width="400px;" src="<?php echo CONF_WEBROOT_URL; ?>images/collection_layouts/layout-3.jpg" /></figure>
									<p><?php echo Labels::getLabel('LBL_Layout_3',$adminLangId);?></p>
								</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="min-height:500px">
							<div class="shop-template fixed-height-div">
								<a rel="facebox" onClick="displayImageInFacebox('<?php echo FatUtility::generateFullUrl(); ?><?php echo CONF_WEBROOT_FRONT_URL; ?>images/collection_layouts/layout-4.jpg');" href="javascript:void(0)">
									<figure class="thumb--square"><img width="400px;" src="<?php echo CONF_WEBROOT_URL; ?>images/collection_layouts/layout-4.jpg" /></figure>
									<p><?php echo Labels::getLabel('LBL_Layout_4',$adminLangId);?></p>
								</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<div class="shop-template fixed-height-div">
								<a rel="facebox" onClick="displayImageInFacebox('<?php echo FatUtility::generateFullUrl(); ?><?php echo CONF_WEBROOT_FRONT_URL; ?>images/collection_layouts/layout-5.jpg');" href="javascript:void(0)">
									<figure class="thumb--square"><img width="400px;" src="<?php echo CONF_WEBROOT_URL; ?>images/collection_layouts/layout-5.jpg" /></figure>
									<p><?php echo Labels::getLabel('LBL_Layout_5',$adminLangId);?></p>
								</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<div class="shop-template fixed-height-div">
								<a rel="facebox" onClick="displayImageInFacebox('<?php echo FatUtility::generateFullUrl(); ?><?php echo CONF_WEBROOT_FRONT_URL; ?>images/collection_layouts/layout-6.jpg');" href="javascript:void(0)">
									<figure class="thumb--square"><img width="400px;" src="<?php echo CONF_WEBROOT_URL; ?>images/collection_layouts/layout-6.jpg" /></figure>
									<p><?php echo Labels::getLabel('LBL_Layout_6',$adminLangId);?></p>
								</a>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
</section>