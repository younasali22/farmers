<?php
defined('SYSTEM_INIT') or die('Invalid Usage.'); 
$frm->setFormTagAttribute('class', 'web_form form_horizontal');
?>
<div class="col-sm-12">
	<h1><?php echo Labels::getLabel('LBL_Collection_Shop_Setup',$adminLangId);?></h1>
	<div class="tabs_nav_container responsive flat">
		<div class="tabs_panel_wrap" style="min-height: 500px;">
			<div class="tabs_panel">
				<?php echo $frm->getFormHtml(); ?>
				<div id="shops_list" class="col-xs-6"></div>
			</div>
			
		</div>
	</div>
</div>
<script type="text/javascript">
$("document").ready(function(){
	$('input[name=\'shops\']').autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: fcom.makeUrl('Shops', 'autocomplete'),
				data: {keyword: request,fIsAjax:1},
				dataType: 'json',
				type: 'post',
				success: function(json) {
					response($.map(json, function(item) {
						return { label: item['name'] ,	value: item['id']	};
					}));
				},
			});
		},
		'select': function( item ) {
			updateCollectionShops(<?php echo $collection_id; ?>, item['value'] );
		}
	});
});
</script>