<?php defined('SYSTEM_INIT') or die('Invalid Usage.');  ?>
<?php
$arr_flds = array(
		'dragdrop'=>'',
		'listserial'=>Labels::getLabel('LBL_Sr._No',$adminLangId),
		'prodcat_identifier'=>Labels::getLabel('LBL_Category_Name',$adminLangId),		
		'child_count' => Labels::getLabel('LBL_Subcategories',$adminLangId),
		'prodcat_active'	=>	Labels::getLabel( 'LBL_Status', $adminLangId ),
		'action' => Labels::getLabel('LBL_Action',$adminLangId),
	);
if(!$canEdit){
	unset($arr_flds['dragdrop']);
}
$tbl = new HtmlElement('table', array('width'=>'100%', 'class'=>'table table-responsive table--hovered','id'=>'prodcat'));
$th = $tbl->appendElement('thead')->appendElement('tr');
foreach ($arr_flds as $val) {
	$e = $th->appendElement('th', array(), $val);
}

$sr_no = $page==1?0:$pageSize*($page-1);
foreach ($arr_listing as $sn=>$row){
	$sr_no++;
	$tr = $tbl->appendElement('tr');
	if($row['prodcat_active'] == applicationConstants::ACTIVE){
		$tr->setAttribute ("id",$row['prodcat_id']);
	}

	if( $row['prodcat_active'] != applicationConstants::ACTIVE ) {
		$tr->setAttribute ("class","nodrag nodrop");
	}
	foreach ($arr_flds as $key=>$val){
		$td = $tr->appendElement('td');
		switch ($key){
			case 'dragdrop':
				if($row['prodcat_active'] == applicationConstants::ACTIVE){
					$td->appendElement('i',array('class'=>'ion-arrow-move icon'));					
					$td->setAttribute ("class",'dragHandle');
				}
			break;
			case 'listserial':
				$td->appendElement('plaintext', array(), $sr_no);
			break;
			case 'prodcat_identifier':
				if($row['prodcat_name']!=''){
					$td->appendElement('plaintext', array(), $row['prodcat_name'],true);
					$td->appendElement('br', array());
					$td->appendElement('plaintext', array(), '('.$row[$key].')',true);
				}else{
					$td->appendElement('plaintext', array(), $row[$key],true);
				}				
				break;
			case 'prodcat_active':
					$active = "";
					if($row['prodcat_active']) {
						$active = 'checked';
					}
					$statucAct = ($canEdit === true) ? 'toggleStatus(event,this)' : '';
					//$str = '<div class="checkbox-switch"><input '.$active.' type="checkbox" id="switch'.$row['prodcat_id'].'" value="'.$row['prodcat_id'].'" onclick="'.$statucAct.'"/><label for="switch'.$row['prodcat_id'].'">Toggle</label></div>';

					$str='<label class="statustab -txt-uppercase">                 
                     <input '.$active.' type="checkbox" id="switch'.$row['prodcat_id'].'" value="'.$row['prodcat_id'].'" onclick="'.$statucAct.'" class="switch-labels"/>
                                      	<i class="switch-handles"></i></label>';
					$td->appendElement('plaintext', array(), $str,true); 
			break;
			case 'child_count': 
				if($row[$key]==0){
					$td->appendElement('plaintext', array(), $row[$key], true);
				}else{
					$td->appendElement('a', array('href'=>FatUtility::generateUrl('ProductCategories','index',array($row['prodcat_id'])),'title'=>Labels::getLabel('LBL_View_Categories',$adminLangId)),$row[$key] );
					//$td->appendElement('a', array('href'=>'javascript:void(0)',"onclick"=>"subcat_list(".$row['prodcat_id'].")",'title'=>Labels::getLabel('LBL_View_Categories',$adminLangId)),$row[$key] );
				} 
			break;

			case 'action':
				$ul = $td->appendElement("ul",array("class"=>"actions actions--centered"));
				if($canEdit){					
					$li = $ul->appendElement("li",array('class'=>'droplink'));
					

					$li->appendElement('a', array('href'=>'javascript:void(0)', 'class'=>'button small green','title'=>Labels::getLabel('LBL_Edit',$adminLangId)),'<i class="ion-android-more-horizontal icon"></i>', true);
              		$innerDiv=$li->appendElement('div',array('class'=>'dropwrap'));
              		$innerUl=$innerDiv->appendElement('ul',array('class'=>'linksvertical'));
              		$innerLiEdit=$innerUl->appendElement('li');

					$innerLiEdit->appendElement('a', array('href'=>'javascript:void(0)', 'class'=>'button small green', 'title'=>Labels::getLabel('LBL_Edit',$adminLangId),"onclick"=>"addCategoryForm(".$row['prodcat_id'].")"),Labels::getLabel('LBL_Edit',$adminLangId), true);

					if( $row['child_count'] > 0){
						/* $li = $ul->appendElement("li");
						$li->appendElement('a', array('href'=>"javascript:void(0)", 'class'=>'button small green', 'title'=>Labels::getLabel('LBL_Content_Block',$adminLangId),"onclick"=>"contentBlock(".$row['prodcat_id'].")"),'<i class="ion-grid icon"></i>', true); */
					}
					
					$innerLiDelete = $innerUl->appendElement("li");
					$innerLiDelete->appendElement('a', array('href'=>"javascript:void(0)", 'class'=>'button small green', 'title'=>Labels::getLabel('LBL_Delete',$adminLangId),"onclick"=>"deleteRecord(".$row['prodcat_id'].")"),Labels::getLabel('LBL_Delete',$adminLangId), true);
					
				}
			break;
			default:
				$td->appendElement('plaintext', array(), $row[$key]);
			break;
		}
	}
}
if (count($arr_listing) == 0){
	$tbl->appendElement('tr')->appendElement('td', array('colspan'=>count($arr_flds)), Labels::getLabel('LBL_No_Records_Found',$adminLangId));
}
echo $tbl->getHtml();
$postedData['page'] = $page;
echo FatUtility::createHiddenFormFromData ( $postedData, array (
		'name' => 'frmCatSearchPaging'
) );
$pagingArr=array('pageCount'=>$pageCount,'page'=>$page,'pageSize'=>$pageSize,'recordCount'=>$pageSize,'adminLangId'=>$adminLangId);
$this->includeTemplate('_partial/pagination.php', $pagingArr,false);
?>
<script>
$(document).ready(function(){
	
	var pcat_id=$('#prodcat_parent').val();
	$('#prodcat').tableDnD({
		onDrop: function (table, row) {
			fcom.displayProcessing();
			var order = $.tableDnD.serialize('id');
			order += '&pcat_id=' + pcat_id;
			fcom.ajax(fcom.makeUrl('productCategories', 'updateOrder'), order, function (res) {
				var ans =$.parseJSON(res);
				if(ans.status==1)
				{	
					fcom.displaySuccessMessage(ans.msg);
				}else{
					fcom.displayErrorMessage(ans.msg);
				}
			});
		},
		dragHandle: ".dragHandle",		
	});
});
</script>