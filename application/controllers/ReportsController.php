<?php
class ReportsController extends LoggedUserController {
	public function __construct( $action ){
		parent::__construct($action);
		$_SESSION[UserAuthentication::SESSION_ELEMENT_NAME]['activeTab'] = 'S';
		if( !User::canAccessSupplierDashboard() ){
			FatApp::redirectUser(CommonHelper::generateUrl('Account','supplierApprovalForm'));
		}
		$this->set('bodyClass','is--dashboard');
	}
	
	public function index(){
		if(User::isSeller()){
			FatApp::redirectUser(CommonHelper::generateUrl('seller'));
		}else if(User::isBuyer()){
			FatApp::redirectUser(CommonHelper::generateUrl('buyer'));
		}else{
			FatApp::redirectUser(CommonHelper::generateUrl(''));
		}
	}
	
	public function productsPerformance(){
		if( !User::canAccessSupplierDashboard() || !User::isSellerVerified(UserAuthentication::getLoggedUserId())){
			FatApp::redirectUser(CommonHelper::generateUrl('Account','supplierApprovalForm'));
		}
		$srchFrm = $this->getProdPerformanceSrchForm();
		$this->set( 'srchFrm', $srchFrm );
		$this->_template->render( );
	}
	
	public function searchProductsPerformance( $orderBy = "DESC", $export = "" ){
		if( !User::canAccessSupplierDashboard() ){
			Message::addErrorMessage( Labels::getLabel("LBL_Invalid_Access!", $this->siteLangId) );
			FatUtility::dieWithError(Message::getHtml());
		}
		
		$post = FatApp::getPostedData();		
		$page = FatApp::getPostedData( 'page', FatUtility::VAR_INT, 1 );
		if ($page < 2) {
			$page = 1;
		}
		$pageSize = FatApp::getConfig('conf_page_size', FatUtility::VAR_INT, 10);				
		$userId = UserAuthentication::getLoggedUserId();
		$shopDetails = Shop::getAttributesByUserId($userId , array('shop_id') , false);
		
		if( !$shopDetails ){
			Message::addErrorMessage( Labels::getLabel("LBL_Invalid_Access!", $this->siteLangId) );
			FatUtility::dieWithError(Message::getHtml());
		}
		
		/* Sub Query to get, how many users added current product in his/her wishlist[ */
		$uWsrch = new UserWishListProductSearch( $this->siteLangId );
		$uWsrch->doNotCalculateRecords();
		$uWsrch->doNotLimitRecords();
		$uWsrch->joinWishLists();
		$uWsrch->addGroupBy('uwlp_selprod_id');
		$uWsrch->addMultipleFields( array( 'uwlp_selprod_id', 'count(uwlist_user_id) as wishlist_user_counts' ) );
		/* ] */
		
		$srch = new OrderProductSearch( $this->siteLangId, true );
		$srch->joinTable( '(' . $uWsrch->getQuery() . ')', 'LEFT OUTER JOIN', 'tquwl.uwlp_selprod_id = op.op_selprod_id', 'tquwl' );
		$srch->addCondition( 'op_shop_id', '=', $shopDetails['shop_id'] );
		//$srch->doNotCalculateRecords();
		$srch->addStatusCondition( unserialize(FatApp::getConfig("CONF_COMPLETED_ORDER_STATUS")) );
		$srch->addCondition( 'order_is_paid', '=', Orders::ORDER_IS_PAID );
		$srch->addMultipleFields(array( 'op_selprod_title', 'op_product_name', 'op_selprod_options', 'op_brand_name', 'SUM(op_qty - op_refund_qty) as totSoldQty', 'op.op_selprod_id', 'IFNULL(tquwl.wishlist_user_counts, 0) as wishlist_user_counts' ));
		$srch->addGroupBy('op.op_selprod_id');
		$srch->addGroupBy('op.op_is_batch');
		$srch->addOrder ( 'totSoldQty', $orderBy );
		
		if( $export == "export" ){
			$srch->doNotCalculateRecords();
			$srch->doNotLimitRecords();
			$rs = $srch->getResultSet();
			$sheetData = array();
			$arr = array( Labels::getLabel('LBL_Product', $this->siteLangId), Labels::getLabel('LBL_Custom_Title', $this->siteLangId), Labels::getLabel('LBL_Options', $this->siteLangId), Labels::getLabel('LBL_Brand', $this->siteLangId), Labels::getLabel('LBL_Sold_Quantity',$this->siteLangId), Labels::getLabel('LBL_WishList_User_Counts', $this->siteLangId));
			array_push( $sheetData, $arr );
			while( $row = FatApp::getDb()->fetch($rs) ){
				$arr = array( $row['op_product_name'], $row['op_selprod_title'], $row['op_selprod_options'],  $row['op_brand_name'], $row['totSoldQty'], $row['wishlist_user_counts'] );
				array_push($sheetData,$arr);
			}
			$csvName = '';
			if( $orderBy == "DESC" ){
				$csvName = Labels::getLabel('LBL_Top_Performing_Products_Report', $this->siteLangId).date("Y-m-d").'.csv';
			} else {
				$csvName = Labels::getLabel('LBL_Bad_Performing_Products_Report', $this->siteLangId).date("Y-m-d").'.csv';
			}
			CommonHelper::convertToCsv( $sheetData, $csvName, ','); exit;
		} else {
			$srch->setPageNumber( $page );
			$srch->setPageSize( $pageSize );
			$rs = $srch->getResultSet();
			$arrListing = FatApp::getDb()->fetchAll($rs);
			$this->set('arrListing', $arrListing);
			$this->set('orderBy', $orderBy);
			$this->set('page', $page);
			$this->set('pageCount', $srch->pages());
			$this->set('recordCount', $srch->recordCount());
			$this->set('postedData', $post);
			$this->_template->render(false, false);
		}
	}
	
	public function searchMostWishListAddedProducts( $export = "" ){
		if( !User::canAccessSupplierDashboard() ){
			Message::addErrorMessage( Labels::getLabel("LBL_Invalid_Access!", $this->siteLangId) );
			FatUtility::dieWithError(Message::getHtml());
		}
		$post = FatApp::getPostedData();
		$page = FatApp::getPostedData( 'page', FatUtility::VAR_INT, 1 );
		if ($page < 2) {
			$page = 1;
		}
		$pageSize = FatApp::getConfig('conf_page_size', FatUtility::VAR_INT, 10);
		$userId = UserAuthentication::getLoggedUserId();
		$shopDetails = Shop::getAttributesByUserId($userId , array('shop_id') , false);
		
		if( !$shopDetails ){
			Message::addErrorMessage( Labels::getLabel("LBL_Invalid_Access!", $this->siteLangId) );
			FatUtility::dieWithError(Message::getHtml());
		}
		
		/* $srch = new ProductSearch( $this->siteLangId );
		$srch->setDefinedCriteria( 0 );
		$srch->joinProductToCategory(); */
		
		/* Sub Query to get, how many users added current product in his/her wishlist[ */
		$uWsrch = new UserWishListProductSearch( $this->siteLangId );
		$uWsrch->doNotCalculateRecords();
		$uWsrch->doNotLimitRecords();
		$uWsrch->joinWishLists();
		$uWsrch->addGroupBy('uwlp_selprod_id');
		$uWsrch->addMultipleFields( array( 'uwlp_selprod_id', 'count(uwlist_user_id) as wishlist_user_counts' ) );
		/* ] */
		
		$srch = SellerProduct::getSearchObject( $this->siteLangId );
		$srch->joinTable( Product::DB_TBL, 'INNER JOIN', 'p.product_id = sp.selprod_product_id', 'p' );
		$srch->joinTable( Product::DB_LANG_TBL, 'LEFT OUTER JOIN', 'p.product_id = p_l.productlang_product_id AND p_l.productlang_lang_id = '.$this->siteLangId, 'p_l' );
		$srch->joinTable( Brand::DB_TBL, 'LEFT OUTER JOIN', 'p.product_brand_id = b.brand_id', 'b' );
		$srch->joinTable( Brand::DB_LANG_TBL, 'LEFT OUTER JOIN', 'b.brand_id = b_l.brandlang_brand_id AND b_l.brandlang_lang_id = '. $this->siteLangId, 'b_l' );
		$srch->joinTable( '(' . $uWsrch->getQuery() . ')', 'LEFT OUTER JOIN', 'tquwl.uwlp_selprod_id = sp.selprod_id', 'tquwl' );
		$srch->addCondition( 'selprod_user_id', '=', $userId );
		$srch->addCondition( 'selprod_deleted', '=', applicationConstants::NO );
		$srch->addCondition( 'wishlist_user_counts', '>', applicationConstants::NO );
		$srch->addOrder('wishlist_user_counts', 'DESC');
		$srch->addMultipleFields( array('selprod_id', 'product_id', 'IFNULL(product_name, product_identifier) as product_name', 'IFNULL(selprod_title  ,IFNULL(product_name, product_identifier)) as selprod_title', 'selprod_active', 'IFNULL(brand_name, brand_identifier) as brand_name', 'IFNULL(tquwl.wishlist_user_counts, 0) as wishlist_user_counts') );
		
		if( $export == "export" ){
			$srch->doNotCalculateRecords();
			$srch->doNotLimitRecords();
			$rs = $srch->getResultSet();
			$sheetData = array();
			$arr = array( Labels::getLabel('LBL_Product', $this->siteLangId), Labels::getLabel('LBL_Custom_Title', $this->siteLangId), Labels::getLabel('LBL_Brand', $this->siteLangId), Labels::getLabel('LBL_User_Counts', $this->siteLangId));
			array_push( $sheetData, $arr );
			while( $row = FatApp::getDb()->fetch($rs) ){
				$arr = array( $row['product_name'], $row['selprod_title'], $row['brand_name'], $row['wishlist_user_counts'] );
				array_push($sheetData,$arr);
			}
			CommonHelper::convertToCsv( $sheetData, Labels::getLabel('LBL_Most_Favorites_Products_Report', $this->siteLangId).date("Y-m-d").'.csv', ','); exit;
		} else {
			$srch->setPageNumber($page);
			$srch->setPageSize($pageSize);
			$rs = $srch->getResultSet();
			
			$arrListing = FatApp::getDb()->fetchAll($rs);
			$this->set( 'arrListing', $arrListing );
			$this->set('pageCount',$srch->pages());
			$this->set('page', $page);
			$this->set('pageSize', $pageSize);
			$this->set('postedData', $post);						
			$this->set('recordCount', $srch->recordCount());
			$this->_template->render(false, false);
		}
	}
	
	public function exportMostWishListAddedProducts(){
		$this->searchMostWishListAddedProducts( "export" );
	}
	
	public function exportProductPerformance( $orderBy = 'DESC' ){
		$this->searchProductsPerformance( $orderBy, "export");
	}
	
	public function productsInventory(){
		if( !User::canAccessSupplierDashboard() ){
			FatApp::redirectUser(CommonHelper::generateUrl('Account','supplierApprovalForm'));
		}
		$frmSrch = $this->getProductInventorySearchForm( $this->siteLangId );
		$this->set( 'frmSrch', $frmSrch );
		$this->_template->render();
	}
	
	public function searchProductsInventory( $export = "" ){
		if( !User::canAccessSupplierDashboard() ){
			Message::addErrorMessage( Labels::getLabel("LBL_Invalid_Access!", $this->siteLangId) );
			FatUtility::dieWithError(Message::getHtml());
		}
		$post = FatApp::getPostedData();
		$pageSize = FatApp::getConfig('CONF_PAGE_SIZE');
		$page = FatApp::getPostedData( 'page', FatUtility::VAR_INT, 0 );
		if( $page < 2 ){
			$page = 1;
		}
		$loggedUserId = UserAuthentication::getLoggedUserId();
		$srch = SellerProduct::getSearchObject( $this->siteLangId );
		$srch->joinTable( Product::DB_TBL, 'INNER JOIN', 'p.product_id = sp.selprod_product_id', 'p' );
		$srch->joinTable( Product::DB_LANG_TBL, 'LEFT OUTER JOIN', 'p.product_id = p_l.productlang_product_id AND p_l.productlang_lang_id = ' . $this->siteLangId, 'p_l' );
		$srch->joinTable( Brand::DB_TBL, 'INNER JOIN', 'p.product_brand_id = b.brand_id', 'b' );
		$srch->joinTable( Brand::DB_LANG_TBL, 'LEFT OUTER JOIN', 'b.brand_id = b_l.brandlang_brand_id  AND brandlang_lang_id = '.$this->siteLangId, 'b_l' );
		$srch->addCondition( 'selprod_user_id', '=', $loggedUserId );
		$srch->addOrder('selprod_active', 'DESC');
		$srch->addOrder('product_name');
		$srch->addMultipleFields(array(
			'selprod_id', 'selprod_user_id', 'selprod_cost', 'selprod_price', 'selprod_stock', 'selprod_product_id',
			'selprod_active', 'selprod_available_from', 'IFNULL(product_name, product_identifier) as product_name', 'IFNULL(selprod_title  ,IFNULL(product_name, product_identifier)) as selprod_title', 'b_l.brand_name'));
			
		if( $keyword = FatApp::getPostedData('keyword')){
			$cnd = $srch->addCondition('product_name' ,'like' , "%$keyword%");
			$cnd->attachCondition( 'selprod_title', 'LIKE', "%$keyword%");
			$cnd->attachCondition( 'brand_name', 'LIKE', "%$keyword%");
		}
		
		if( $export == "export" ){
			$srch->doNotCalculateRecords();
			$srch->doNotLimitRecords();
			$rs = $srch->getResultSet();
			$sheetData = array();
			$arr = array(Labels::getLabel('LBL_Product', $this->siteLangId), Labels::getLabel('LBL_Custom_Title(If_Any)', $this->siteLangId), Labels::getLabel('LBL_Brand', $this->siteLangId), Labels::getLabel('LBL_Stock_Quantity', $this->siteLangId));
			array_push( $sheetData, $arr );
			while( $row = FatApp::getDb()->fetch($rs) ){
				$arr = array( $row['product_name'], $row['selprod_title'], $row['brand_name'], $row['selprod_stock'] );
				array_push($sheetData,$arr);
			}
			CommonHelper::convertToCsv( $sheetData, Labels::getLabel('LBL_Products_Inventory_Report', $this->siteLangId).date("Y-m-d").'.csv', ','); exit;
		} else {
			$srch->setPageNumber( $page );
			$srch->setPageSize( $pageSize );
			$rs = $srch->getResultSet();
			$arrListing = FatApp::getDb()->fetchAll($rs);
			$this->set( 'page', $page );
			$this->set( 'pageSize', $pageSize );
			$this->set( 'pageCount', $srch->pages() );
			$this->set( 'postedData', $post );
			$this->set( 'recordCount', $srch->recordCount() );
			$this->set( 'arrListing', $arrListing );
			$this->_template->render( false, false );
		}
	}
	
	public function exportProductsInventoryReport(){
		$this->searchProductsInventory( "export" );
	}
	
	public function productsInventoryStockStatus(){
		if( !User::canAccessSupplierDashboard() ){
			FatApp::redirectUser(CommonHelper::generateUrl('Account','supplierApprovalForm'));
		}
		$frmSrch = $this->getProductInventoryStockStatusSearchForm( $this->siteLangId );
		$this->set( 'frmSrch', $frmSrch );
		$this->_template->render();
	}
	
	public function searchProductsInventoryStockStatus( $export = "" ){
		if( !User::canAccessSupplierDashboard() ){
			Message::addErrorMessage( Labels::getLabel("LBL_Invalid_Access!", $this->siteLangId) );
			FatUtility::dieWithError(Message::getHtml());
		}
		$post = FatApp::getPostedData();
		$pageSize = FatApp::getConfig('CONF_PAGE_SIZE');
		$page = FatApp::getPostedData( 'page', FatUtility::VAR_INT, 0 );
		if( $page < 2 ){
			$page = 1;
		}
		
		$loggedUserId = UserAuthentication::getLoggedUserId();
		
		/* [ */
		$orderProductSrch = new OrderProductSearch( $this->siteLangId, true );
		$orderProductSrch->joinPaymentMethod();
		$orderProductSrch->doNotCalculateRecords();
		$orderProductSrch->doNotLimitRecords();
		$orderProductSrch->addStatusCondition( unserialize(FatApp::getConfig("CONF_PRODUCT_IS_ON_ORDER_STATUSES")) );
		$cnd = $orderProductSrch->addCondition( 'order_is_paid', '=', Orders::ORDER_IS_PAID );
		$cnd->attachCondition( 'pm.pmethod_code', '=', 'CashOnDelivery' );
		$orderProductSrch->addCondition( 'op.op_is_batch', '=', 0 );
		$orderProductSrch->addMultipleFields(array( 'op.op_selprod_id', 'SUM(op_qty) as stock_on_order', 'op_selprod_options' ));
		$orderProductSrch->addGroupBy('op.op_selprod_id');
		/* ] */
		
		$srch = SellerProduct::getSearchObject( $this->siteLangId );
		$srch->joinTable( '('. $orderProductSrch->getQuery() .')', 'LEFT OUTER JOIN', 'sp.selprod_id = qryop.op_selprod_id', 'qryop' ); 
		$srch->joinTable( Product::DB_TBL, 'INNER JOIN', 'p.product_id = sp.selprod_product_id', 'p' );
		$srch->joinTable( Product::DB_LANG_TBL, 'LEFT OUTER JOIN', 'p.product_id = p_l.productlang_product_id AND p_l.productlang_lang_id = ' . $this->siteLangId, 'p_l' );
		$srch->joinTable( Brand::DB_TBL, 'INNER JOIN', 'p.product_brand_id = b.brand_id', 'b' );
		$srch->joinTable( Brand::DB_LANG_TBL, 'LEFT OUTER JOIN', 'b.brand_id = b_l.brandlang_brand_id  AND brandlang_lang_id = '.$this->siteLangId, 'b_l' );
		$srch->addCondition( 'selprod_user_id', '=', $loggedUserId );
		$srch->addOrder( 'selprod_active', 'DESC' );
		$srch->addOrder('product_name');
		$srch->addMultipleFields(array(
			'selprod_id', 'selprod_user_id', 'selprod_cost', 'selprod_price', 'selprod_stock', 'selprod_product_id',
			'selprod_active', 'selprod_available_from', 'IFNULL(product_name, product_identifier) as product_name', 'IFNULL(selprod_title  ,IFNULL(product_name, product_identifier)) as selprod_title', 
			'b_l.brand_name', 'IFNULL(qryop.stock_on_order, 0) as stock_on_order'));
			
		if( $keyword = FatApp::getPostedData('keyword') ){
			$cnd = $srch->addCondition('product_name' ,'like' , "%$keyword%");
			$cnd->attachCondition( 'selprod_title', 'LIKE', "%$keyword%");
			$cnd->attachCondition( 'brand_name', 'LIKE', "%$keyword%");
		}
		
		if( $export == "export" ){
			$srch->doNotCalculateRecords();
			$srch->doNotLimitRecords();
			$rs = $srch->getResultSet();
			$sheetData = array();
			$arr = array(Labels::getLabel('LBL_Product', $this->siteLangId),Labels::getLabel('LBL_Custom_title(if_any)', $this->siteLangId), Labels::getLabel('LBL_Brand', $this->siteLangId), Labels::getLabel('LBL_Stock_Available', $this->siteLangId), Labels::getLabel('LBL_Stock_on_order', $this->siteLangId),Labels::getLabel('LBL_Unit_Price', $this->siteLangId), Labels::getLabel('LBL_Total_Value(Stock_Available*unit_Price)', $this->siteLangId));
			array_push( $sheetData, $arr );
			/* while( $row = FatApp::getDb()->fetch($rs) ){
				$arr = array( $row['product_name'], $row['selprod_title'], $row['brand_name'], $row['selprod_stock'] );
				array_push($sheetData,$arr);
			} */
			CommonHelper::convertToCsv( $sheetData, Labels::getLabel('LBL_Products_Inventory_Report', $this->siteLangId).date("Y-m-d").'.csv', ','); exit;
		} else {
			$srch->setPageNumber( $page );
			$srch->setPageSize( $pageSize );
			$rs = $srch->getResultSet();
			$arrListing = FatApp::getDb()->fetchAll( $rs );
			$this->set( 'arrListing', $arrListing );
			$this->set( 'page', $page );
			$this->set( 'pageSize', $pageSize );
			$this->set( 'pageCount', $srch->pages() );
			$this->set( 'postedData', $post );
			$this->set( 'recordCount', $srch->recordCount() );
			$this->_template->render( false, false );
		}
	}
	
	public function exportProductsInventoryStockStatusReport(){
		$this->searchProductsInventoryStockStatus( "export" );
	}
	
	private function getProductInventorySearchForm( $langId ){
		$frm = new Form('frmProductInventorySrch');
		$frm->addTextBox( '', 'keyword', '' );
		$frm->addHiddenField('','page');
		$fldSubmit = $frm->addSubmitButton( '', 'btn_submit', Labels::getLabel('LBL_Search',$langId) );
		$fldCancel = $frm->addButton( "", "btn_clear", Labels::getLabel("LBL_Clear", $langId), array('onclick'=>'clearSearch();') );
		$fldSubmit->attachField( $fldCancel );
		return $frm;
	}
	
	private function getProductInventoryStockStatusSearchForm( $langId ){
		$frm = new Form('frmProductInventoryStockStatusSrch');
		$frm->addTextBox( '', 'keyword', '' );
		$frm->addHiddenField('','page');
		$fldSubmit = $frm->addSubmitButton( '', 'btn_submit', Labels::getLabel('LBL_Search',$langId) );
		$fldCancel = $frm->addButton( "", "btn_clear", Labels::getLabel("LBL_Clear", $langId), array('onclick'=>'clearSearch();') );
		$fldSubmit->attachField( $fldCancel );
		return $frm;
	}
	
	private function getProdPerformanceSrchForm(){
		$frm = new Form('frmProdPerformanceSrch');
		//$frm->addHiddenField('','order_by');
		return $frm;
	}
}
?>