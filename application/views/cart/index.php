<?php defined('SYSTEM_INIT') or die('Invalid Usage.'); ?>
<div id="body" class="body bg--gray">
    <section class="top-space">
		<div class="fixed-container">
			<div class="panel panel--centered" id="cartList"></div>
		</div>
	</section>
	<div class="gap"></div>
</div>