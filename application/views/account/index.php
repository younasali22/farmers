<div id="body" class="body bg--gray">
    <section class="dashboard">
		<?php $this->includeTemplate('_partial/dashboardTop.php'); ?>  
		<div class="fixed-container">
			<div class="row">
				<?php $this->includeTemplate('_partial/dashboardNavigation.php'); ?>
				<div class="col-md-10 panel__right--full" >
					Buyer dashboard page
				</div>
			</div>
		</div>
	</section>
	<div class="gap"></div>
</div>