<div class="box__head">
		   <h4><?php echo Labels::getLabel('LBL_Add_Custom_Product',$siteLangId); ?></h4>
				<div class="group--btns panel__head_action">
				<a href="<?php echo CommonHelper::generateUrl('seller','products');?>" class="btn btn--primary btn--sm "><strong><?php echo Labels::getLabel( 'LBL_Back_To_My_Products', $siteLangId)?></strong> </a>				
				<a href="<?php echo CommonHelper::generateUrl('seller','customProduct');?>" class="btn btn--secondary btn--sm "><strong><?php echo Labels::getLabel( 'LBL_Manage_Your_Catalog', $siteLangId)?></strong> </a>
			</div>
	</div>