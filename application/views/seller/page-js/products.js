$(document).ready(function(){
	loadSellerProducts(document.frmSearchSellerProducts);
});

(function() {
	var runningAjaxReq = false;
	//var dv = '#sellerProductsForm';
	var dv = '#listing';
	
	checkRunningAjax = function(){
		if( runningAjaxReq == true ){
			console.log(runningAjaxMsg);
			return;
		}
		runningAjaxReq = true;
	};
	
	loadSellerProducts = function(frm){
		sellerProducts($( frm.product_id ).val());
	};
	
	sellerProducts = function(product_id){
		$('#listing').html(fcom.getLoader());
		/* if product id is not passed, then it will become or will fetch custom products of that seller. */
		if( typeof product_id == undefined || product_id == null ){
			product_id = 0;
		}
		var data = fcom.frmData(document.frmSearch);
		fcom.ajax(fcom.makeUrl('Seller', 'sellerProducts', [ product_id ]), data, function(t) {
			$('#listing').html(t);
		});
	}
	
	goToSellerProductSearchPage = function(page) {
		if(typeof page==undefined || page == null){
			page =1;
		}
		var frm = document.frmSearch;		
		$(frm.page).val(page);
		loadSellerProducts(frm);
	}
	sellerProductForm = function(product_id, selprod_id){
		$(dv).html(fcom.getLoader());
		fcom.ajax(fcom.makeUrl('Seller', 'sellerProductForm', [ product_id, selprod_id ]), '', function(t) {
			$(dv).html(t);
		});
	}; 
	sellerProductDelete=function(id){
		if(!confirm(langLbl.confirmDelete)){return;}
		data='id='+id;
		fcom.updateWithAjax(fcom.makeUrl('Seller','sellerProductDelete'),data,function(res){		
			loadSellerProducts(document.frmSearchSellerProducts);
		});
	};

	clearSearch = function(){
		document.frmSearch.reset();
		loadSellerProducts(document.frmSearchSellerProducts);
	};
	
})();	