(function() {
	var runningAjaxMsg = 'some requests already running or this stucked into runningAjaxReq variable value, so try to relaod the page and update the same to WebMaster. ';
	var runningAjaxReq = false;
	var dv = '#listing';
	
	checkRunningAjax = function(){
		if( runningAjaxReq == true ){
			console.log(runningAjaxMsg);
			return;
		}
		runningAjaxReq = true;
	};
	
	searchCatalogProducts = function(frm){ 
		checkRunningAjax();
		/*[ this block should be written before overriding html of 'form's parent div/element, otherwise it will through exception in ie due to form being removed from div */
		var data = fcom.frmData(frm);
		/*]*/		
		$(dv).html( fcom.getLoader() );
		
		fcom.ajax(fcom.makeUrl('Seller','searchCatalogProduct'),data,function(res){
			runningAjaxReq = false;
			$(dv).html(res);
		});
	};
	
	goToCatalogProductSearchPage = function(page){
		if(typeof page==undefined || page == null){
			page = 1;
		}
		var frm = document.frmCatalogProductSearchPaging;		
		$(frm.page).val(page);
		searchCatalogProducts(frm);
	}
	
	setShippedBySeller = function(product_id){
		var data = 'shippedBy=seller&product_id='+product_id;
		fcom.updateWithAjax(fcom.makeUrl('Seller', 'setUpshippedBy'), data, function(t) {									
			searchCatalogProducts(document.frmSearchCatalogProduct);
		});
	};
	
	
	setShippedByAdmin = function(product_id){
		var data = 'shippedBy=admin&product_id='+product_id;
		fcom.updateWithAjax(fcom.makeUrl('Seller', 'setUpshippedBy'), data, function(t) {									
			searchCatalogProducts(document.frmSearchCatalogProduct);
		});
	};
	
	sellerShippingForm = function(productId){
		$(dv).html( fcom.getLoader() );
		
		fcom.ajax(fcom.makeUrl('Seller','sellerShippingForm',[productId]),'',function(res){
			runningAjaxReq = false;
			$(dv).html(res);
		});
	}
	
	addShippingTab = function(id,sellerId){
		ShipDiv = "#tab_shipping";
		fcom.ajax(fcom.makeUrl('seller','getShippingTab'),'product_id='+id,function(t){
			try{
					res= jQuery.parseJSON(t);
					$.facebox(res.msg,'faceboxWidth');
				}catch (e){
					
					$(ShipDiv).html(t);
				}			
			
		});
	}
	
	setupSellerShipping = function(frm){
		if (!$(frm).validate()) return;
		if( runningAjaxReq == true ){
			console.log(runningAjaxMsg);
			return;
		}
				var data = fcom.frmData(frm);
	
		fcom.updateWithAjax(fcom.makeUrl('Seller', 'setupSellerShipping'), (data), function(t) {
			runningAjaxReq = false;
		
			
			
			productId =  t.product_id;
			searchCatalogProducts();
			
		});
	}
	
	clearSearch = function(){
		document.frmSearchCatalogProduct.reset();
		searchCatalogProducts(document.frmSearchCatalogProduct);
	};
	
	catalogInfo = function(product_id) {
		$.facebox(function() {
			fcom.ajax(fcom.makeUrl('Seller','catalogInfo',[product_id]), '', function(t){
				$.facebox(t,'faceboxWidth catalogInfo');
			});
		});
	}
	
	shippingautocomplete = function(shipping_row) {
	
		$('input[name=\'product_shipping[' + shipping_row + '][country_name]\']').focusout(function() {
				setTimeout(function(){ $('.suggestions').hide(); }, 500); 
		});
		
		$('input[name=\'product_shipping[' + shipping_row + '][company_name]\']').focusout(function() {
				setTimeout(function(){ $('.suggestions').hide(); }, 500);
		});
		
		$('input[name=\'product_shipping[' + shipping_row + '][processing_time]\']').focusout(function() {
				setTimeout(function(){ $('.suggestions').hide(); }, 500);
		});
		$('input[name=\'product_shipping[' + shipping_row + '][country_name]\']').autocomplete({
			'source': function(request, response) {
			
				$.ajax({
					url: fcom.makeUrl('seller', 'countries_autocomplete'),
					data: {keyword: request,fIsAjax:1,includeEverywhere:true},
					dataType: 'json',
					type: 'post',
					success: function(json) {
						
						response($.map(json, function(item) {
						
							return { 
								label: item['name'] ,
								value: item['id']
								};
						}));
					},
				});
			},
			'select': function(item) {
				
				$('input[name=\'product_shipping[' + shipping_row + '][country_name]').val(item.label);
				$('input[name=\'product_shipping[' + shipping_row + '][country_id]').val(item.value);
				
				
			}
		});
		
		
		$('input[name=\'product_shipping[' + shipping_row + '][company_name]\']').autocomplete({
				'source': function(request, response) {
			
				$.ajax({
					url: fcom.makeUrl('seller', 'shippingCompanyAutocomplete'),
					data: {keyword: request,fIsAjax:1},
					dataType: 'json',
					type: 'post',
					success: function(json) {
						
						response($.map(json, function(item) {
						
							return { 
								label: item['name'] ,
								value: item['id']
								};
						}));
					},
				});
			},
			'select': function(item) {
				
				$('input[name=\'product_shipping[' + shipping_row + '][company_name]\']').val(item.label);
					$('input[name=\'product_shipping[' + shipping_row + '][company_id]\']').val(item.value);
				
				
			}
				
		});
		
		$('input[name=\'product_shipping[' + shipping_row + '][processing_time]\']').autocomplete({
				'source': function(request, response) {
			
				$.ajax({
					url: fcom.makeUrl('seller', 'shippingMethodDurationAutocomplete'),
					data: {keyword: request,fIsAjax:1},
					dataType: 'json',
					type: 'post',
					success: function(json) {
						
						response($.map(json, function(item) {
						
							return { 
								label: item['name']+'['+ item['duraion']+']' ,
								value: item['id']
								};
						}));
					},
				});
			},
			'select': function(item) {
				
				$('input[name=\'product_shipping[' + shipping_row + '][processing_time]\']').val(item.label);
					$('input[name=\'product_shipping[' + shipping_row + '][processing_time_id]\']').val(item.value);
				
				
			}
				
		});
	
	}
})();