<?php defined('SYSTEM_INIT') or die('Invalid Usage.');?>
<div class="box__head box__head--large">
   <h4><?php echo Labels::getLabel('LBL_Product_Listing',$siteLangId); ?></h4>
	<div class="group--btns">
		<a class="btn btn--primary btn--sm" href="javascript:void(0); " onClick="sellerProductSpecialPriceForm(<?php echo $selprod_id; ?>, 0);"><?php echo Labels::getLabel( 'LBL_Add_New_Special_Price', $siteLangId)?></a>	
	</div>
</div>
<div class="box__body">	
	<div class="tabs tabs--small tabs--offset tabs--scroll clearfix">
		<?php require_once('sellerCatalogProductTop.php');?>
	</div>
	<div class="tabs__content form">	
		<div class="form__content">	
			<div class="col-md-12">
				<div class="form__subcontent">						
				<?php 
				$arr_flds = array(
					'listserial'=> Labels::getLabel( 'LBL_Sr.', $siteLangId ),
					'splprice_price' => Labels::getLabel( 'LBL_Special_Price', $siteLangId ),
					'splprice_start_date' => Labels::getLabel( 'LBL_Start_Date', $siteLangId ),
					'splprice_end_date' => Labels::getLabel( 'LBL_End_Date', $siteLangId ),
					'action'	=>	Labels::getLabel('LBL_Action', $siteLangId),
				);
				$tbl = new HtmlElement('table', array('width'=>'100%', 'class'=>'table'));
				$th = $tbl->appendElement('thead')->appendElement('tr',array('class' => ''));
				foreach ($arr_flds as $val) {
					$e = $th->appendElement('th', array(), $val);
				}

				$sr_no = 0;
				foreach ($arrListing as $sn => $row){
					$sr_no++;
					$tr = $tbl->appendElement('tr',array());

					foreach ($arr_flds as $key=>$val){
						$td = $tr->appendElement('td');
						switch ($key){
							case 'listserial':
								$td->appendElement('plaintext', array(), '<span class="caption--td">'.$val.'</span>'.$sr_no,true);
							break;
							case 'splprice_price':
								$td->appendElement( 'plaintext', array(), '<span class="caption--td">'.$val.'</span>'.CommonHelper::displayMoneyFormat($row[$key] ,true,true ),true );
							break;
							case 'splprice_start_date';
								$td->appendElement( 'plaintext', array(), '<span class="caption--td">'.$val.'</span>'.FatDate::format($row[$key]),true );
							break;
							case 'splprice_end_date';
								$td->appendElement( 'plaintext', array(), '<span class="caption--td">'.$val.'</span>'.FatDate::format($row[$key]),true );
							break;
							case 'action':
								$ul = $td->appendElement("ul",array("class"=>"actions"),'<span class="caption--td">'.$val.'</span>',true);
								$li = $ul->appendElement("li");
								$li->appendElement('a', array('href'=>'javascript:void(0)', 'class'=>'',
								'title'=>Labels::getLabel('LBL_Edit',$siteLangId),"onclick"=>"sellerProductSpecialPriceForm(".$selprod_id.", ".$row['splprice_id'].")"),
								'<i class="fa fa-edit"></i>', true);
								
								$li = $ul->appendElement("li");
								$li->appendElement('a', array('href'=>'javascript:void(0)', 'class'=>'',
								'title'=>Labels::getLabel('LBL_Delete',$siteLangId),"onclick"=>"deleteSellerProductSpecialPrice(".$row['splprice_id'].")"),
								'<i class="fa fa-trash"></i>', true);
							break;
							default:
								$td->appendElement('plaintext', array(), '<span class="caption--td">'.$val.'</span>'.$row[$key],true);
							break;
						}
					}
				}
				if (count($arrListing) == 0){
					// $tbl->appendElement('tr')->appendElement('td', array('colspan'=>count($arr_flds)), Labels::getLabel('LBL_No_Special_Price_added_to_this_product', $siteLangId));
					$this->includeTemplate('_partial/no-record-found.php',array('siteLangId' => $siteLangId),false);
				}
				else{
					echo $tbl->getHtml();
				}
				?>
				</div>	
			</div>	
		</div>		
	</div>
</div>