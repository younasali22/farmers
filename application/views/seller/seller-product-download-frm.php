<?php defined('SYSTEM_INIT') or die('Invalid Usage.'); ?>

<div class="box__body">	
	<div class="tabs tabs--small tabs--offset tabs--scroll clearfix">
		<?php require_once('sellerCatalogProductTop.php');?>
	</div>
	<div class="tabs__content form">
		<div class="form__content">
			<div class="col-md-12">
				<?php				
				$selprodDownloadFrm->setFormTagAttribute('id', 'frmDownload');
				$selprodDownloadFrm->setFormTagAttribute('class','form form--horizontal');
				$selprodDownloadFrm->developerTags['colClassPrefix'] = 'col-md-';
				$selprodDownloadFrm->developerTags['fld_default_col'] = 6; 
				$img_fld = $selprodDownloadFrm->getField('downloadable_file');
				$img_fld->setFieldTagAttribute( 'onchange','setUpSellerProductDownloads(); return false;');
				echo $selprodDownloadFrm->getFormHtml(); ?>
			</div>
			<div class="col-md-12">
				<?php 
				$arr_flds = array(
					'listserial'=>Labels::getLabel('LBL_Sr_No.', $siteLangId),
					'afile_name' => Labels::getLabel('LBL_File', $siteLangId),
					'afile_lang_id' => Labels::getLabel('LBL_Language', $siteLangId),
					'action' => Labels::getLabel('LBL_Action', $siteLangId),					
				);
				
				$tbl = new HtmlElement('table', array('width'=>'100%', 'class'=>'table'));
				$th = $tbl->appendElement('thead')->appendElement('tr',array('class' => ''));
				foreach ($arr_flds as $val) {
					$e = $th->appendElement('th', array(), $val);
				}

				$sr_no = 0;
				foreach ($attachments as $sn => $row){
					$sr_no++;
					$tr = $tbl->appendElement('tr');

					foreach ($arr_flds as $key=>$val){
						$td = $tr->appendElement('td');
						switch ($key){
							case 'listserial':
								$td->appendElement('plaintext', array(), '<span class="caption--td">'.$val.'</span>'.$sr_no,true);
							break;
							case 'afile_lang_id':
								$lang_name = Labels::getLabel('LBL_All',$siteLangId);
								if( $row['afile_lang_id'] > 0 ){
									$lang_name = $languages[$row['afile_lang_id']];
								}
								$td->appendElement('plaintext', array(),  '<span class="caption--td">'.$val.'</span>'.$lang_name, true);
							break;
							case 'afile_name':
								$fileName = '<a target="_blank" href="'.CommonHelper::generateUrl('seller','downloadDigitalFile',array($row['afile_id'],$row['afile_record_id'])).'">'.$row[$key].'</a>';
								$td->appendElement('plaintext', array(),  '<span class="caption--td">'.$val.'</span>'.$fileName, true);
							break;
							case 'action':
								$ul = $td->appendElement("ul",array("class"=>"actions"),'<span class="caption--td">'.$val.'</span>',true);
								
								$li = $ul->appendElement("li");
								$li->appendElement("a", array('title' => Labels::getLabel('LBL_Product_Images', $siteLangId),
								'onclick' => 'deleteDigitalFile('.$row['afile_record_id'].','.$row['afile_id'].')', 'href'=>'javascript:void(0)'),
								'<i class="fa fa-trash"></i>', true);

							break;
							default:
								$td->appendElement('plaintext', array(), '<span class="caption--td">'.$val.'</span>'.$row[$key],true);
							break;
						}
					}
				}
				if( !empty($attachments) ){					
					echo $tbl->getHtml();				
				}
				?>			
			</div>			
		</div>
	</div>
</div>	
<?php echo FatUtility::createHiddenFormFromData ( array('product_id'=>$product_id,'product_type'=>$product_type), array ('name' => 'frmSearchSellerProducts') );?>
