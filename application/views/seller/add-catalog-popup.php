<?php  defined('SYSTEM_INIT') or die('Invalid Usage.');?>
<div id="" >
  <div class="">
    <div class="product-options">
      <div class="popup-title"><h3><?php echo Labels::getLabel('LBL_Catalog',$siteLangId);?></h3></div>
      <ul>
        <?php if( User::canAddCustomProduct() ){ ?>
        <li  data-heading="OR" ><a href="<?php echo CommonHelper::generateUrl('seller','customProductForm' );?>"><i class="icn fa  fa-camera"></i>
          <p><?php echo Labels::getLabel('LBL_Create_new_catalog',$siteLangId);?> </p>
          <span><?php echo Labels::getLabel('LBL_Create_your_Catalog',$siteLangId);?></span> </a> </li>
        <?php } else if((isset($canAddCustomProduct) && $canAddCustomProduct==false) && (isset($canRequestProduct) && $canRequestProduct === true )){ ?>
        <li  data-heading="OR"><a href="<?php echo CommonHelper::generateUrl('Seller','requestedCatalog');?>" class="btn btn--primary btn--sm"><i class="icn fa fa-file-text "></i>
          <p><?php echo Labels::getLabel('LBL_Request_A_Product',$siteLangId);?></p>
          <span><?php echo Labels::getLabel('LBL_Request_to_add_a_new_product_in_catalog',$siteLangId);?></span></a></li>
        <?php } ?>
        <li  data-heading="OR"><a href="<?php echo CommonHelper::generateUrl('seller','catalog');?>"><i class="icn fa fa-camera-retro"></i>
          <p><?php echo Labels::getLabel('LBL_Search_and_add_existing_Products',$siteLangId);?></p>
          <span><?php echo Labels::getLabel('LBL_Search_and_pick_to_sell_products_from_existing_catalog',$siteLangId);?></span> </a> </li>
        <li  data-heading="OR"><a href="<?php echo CommonHelper::generateUrl('seller','InventoryUpdate'); ?>"><i class="icn fa fa-file-text-o"></i>
          <p><?php echo Labels::getLabel('LBL_Inventory_Update_in_Bulk',$siteLangId);?></p>
          <span><?php echo Labels::getLabel('LBL_update_inventory_of_your_existing_products',$siteLangId);?></span> </a></li>
      </ul>
    </div>
  </div>
</div>
