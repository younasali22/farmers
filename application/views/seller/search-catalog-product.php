<?php
defined('SYSTEM_INIT') or die('Invalid Usage.');
$arr_flds = array(
	'listserial'=>'Sr.',
	'product_identifier' => Labels::getLabel('LBL_Product', $siteLangId),
	//'attrgrp_name' => Labels::getLabel('LBL_Attribute_Group', $siteLangId),
	'product_model' => Labels::getLabel('LBL_Model', $siteLangId),
	'product_shipped_by' => Labels::getLabel('LBL_Shipped_by_me', $siteLangId),
	'action' => Labels::getLabel('LBL_Action', $siteLangId)
);
$tbl = new HtmlElement('table', array('width'=>'100%', 'class'=>'table'));
$th = $tbl->appendElement('thead')->appendElement('tr',array('class' => ''));
foreach ($arr_flds as $val) {
	$e = $th->appendElement('th', array(), $val);
}

$sr_no = ($page == 1) ? 0 : ($pageSize*($page-1));
foreach ($arr_listing as $sn => $row){
	$sr_no++;
	$tr = $tbl->appendElement('tr',array('class' => ''));

	foreach ($arr_flds as $key=>$val){
		$td = $tr->appendElement('td');
		switch ($key){
			case 'listserial':
				$td->appendElement('plaintext', array(), '<span class="caption--td">'.$val.'</span>'.$sr_no,true);
			break;
			case 'product_identifier':
				$td->appendElement('plaintext', array(), '<span class="caption--td">'.$val.'</span>'.$row['product_name'] . '<br>', true);
				$td->appendElement('plaintext', array(), '('.$row[$key].')', true);
			break;
			case 'attrgrp_name':
				$td->appendElement('plaintext', array(), '<span class="caption--td">'.$val.'</span>'.CommonHelper::displayNotApplicable($siteLangId, $row[$key]),true);
			break;			
			case 'product_shipped_by':
				$active = "";
				if($row['psbs_user_id']){
					$active = 'checked';
				}
				
				$str =  Labels::getLabel('LBL_N/A',$siteLangId);
				if(!$row['product_seller_id'] && $row['product_type'] != Product::PRODUCT_TYPE_DIGITAL){
					$statucAct = (!$row['psbs_user_id']) ? 'setShippedBySeller('.$row['product_id'].')' : 'setShippedByAdmin('.$row['product_id'].')' ;

					$str = '<div class="checkbox-switch"><input '.$active.' type="checkbox" id="switch'.$row['product_id'].'" onclick="'.$statucAct.'"/><label for="switch'.$row['product_id'].'">Toggle</label></div>';
				}
				$td->appendElement('plaintext', array(), $str,true);
			break;
			case 'action':
				/* $ul = $td->appendElement("ul",array('class'=>''),'<span class="caption--td">'.$val.'</span>',true);
				$li = $ul->appendElement("li"); */
				$td->appendElement('a', array('href'=>CommonHelper::generateUrl('Seller','sellerProductForm',array($row['product_id'])), 'class'=>'btn btn--primary btn--sm','title'=>Labels::getLabel('LBL_Pick_to_Sell',$siteLangId)),
				Labels::getLabel('LBL_Pick_to_Sell',$siteLangId), true);
				$td->appendElement('plaintext',array(),'&nbsp;&nbsp;',true);
				$td->appendElement('a', array('href'=>'javascript:void(0)', 'onclick'=>'catalogInfo('.$row['product_id'].')', 'class'=>'btn btn--secondary btn--sm','title'=>Labels::getLabel('LBL_product_Info',$siteLangId)),
				Labels::getLabel('LBL_Catalog_Info',$siteLangId), true);
				
				if($row['product_added_by_admin_id'] && $row['psbs_user_id'] && $row['product_type'] == PRODUCT::PRODUCT_TYPE_PHYSICAL){
					$td->appendElement('plaintext',array(),'&nbsp;&nbsp;',true);
					$td->appendElement('a', array('href'=>'javascript:void(0)','onclick'=>'sellerShippingForm('.$row['product_id'].')', 'class'=>'link--arrow','title'=>'&nbsp;'.Labels::getLabel('LBL_Edit_Shipping',$siteLangId),true),
					Labels::getLabel('LBL_Edit_Shipping',$siteLangId), true);	
				} 
			break;
			default:
				$td->appendElement('plaintext', array(), '<span class="caption--td">'.$val.'</span>'.$row[$key],true);
			break;
		}
	}
}
if (count($arr_listing) == 0){
	$message = Labels::getLabel('LBL_Searched_product_is_not_found_in_catalog', $siteLangId);
	$linkArr = array();
	if( User::canAddCustomProductAvailableToAllSellers() ){
	$linkArr = array(
		0=>array(
			'href'=>CommonHelper::generateUrl('Seller','CustomCatalogProductForm'),
			'label'=>Labels::getLabel('LBL_Create_New_Product', $siteLangId),
			)	
		);
	}
	$this->includeTemplate('_partial/no-record-found.php' , array('siteLangId'=>$siteLangId,'linkArr'=>$linkArr,'message'=>$message));
	//$tbl->appendElement('tr')->appendElement('td', array('colspan'=>count($arr_flds)), Labels::getLabel('LBL_No_products_found', $siteLangId));
}else{
	echo $tbl->getHtml();
}
$postedData['page'] = $page;
echo FatUtility::createHiddenFormFromData ( $postedData, array ('name' => 'frmCatalogProductSearchPaging') );

$pagingArr=array('pageCount'=>$pageCount,'page'=>$page,'callBackJsFunc' => 'goToCatalogProductSearchPage');
$this->includeTemplate('_partial/pagination.php', $pagingArr,false);
