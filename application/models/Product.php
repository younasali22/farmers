<?php
class Product extends MyAppModel{
	const DB_TBL = 'tbl_products';
	const DB_LANG_TBL ='tbl_products_lang';
	const DB_TBL_PREFIX = 'product_';

	const DB_NUMERIC_ATTRIBUTES_TBL = 'tbl_product_numeric_attributes';
	const DB_NUMERIC_ATTRIBUTES_PREFIX = 'prodnumattr_';

	const DB_TEXT_ATTRIBUTES_TBL = 'tbl_product_text_attributes';
	const DB_TEXT_ATTRIBUTES_PREFIX = 'prodtxtattr_';

	const DB_TBL_PRODUCT_TO_CATEGORY = 'tbl_product_to_category';
	const DB_TBL_PRODUCT_TO_CATEGORY_PREFIX = 'ptc_';

	const DB_PRODUCT_TO_OPTION = 'tbl_product_to_options';
	const DB_PRODUCT_TO_OPTION_PREFIX = 'prodoption_';
	
	const DB_PRODUCT_TO_SHIP = 'tbl_product_shipping_rates';
	const DB_PRODUCT_TO_SHIP_PREFIX = 'pship_';
	
	const DB_PRODUCT_TO_TAG = 'tbl_product_to_tags';
	const DB_PRODUCT_TO_TAG_PREFIX = 'ptt_';
	
	const DB_TBL_PRODUCT_FAVORITE = 'tbl_user_favourite_products';

	const DB_PRODUCT_SPECIFICATION = 'tbl_product_specifications';
	const DB_PRODUCT_SPECIFICATION_PREFIX = 'prodspec_';
	
	const DB_PRODUCT_LANG_SPECIFICATION = 'tbl_product_specifications_lang';
	const DB_PRODUCT_LANG_SPECIFICATION_PREFIX = 'prodspeclang_';
	
	const DB_TBL_PRODUCT_SHIPPING = 'tbl_products_shipping';
	const DB_TBL_PRODUCT_SHIPPING_PREFIX = 'ps_';
	
	const DB_PRODUCT_SHIPPED_BY_SELLER = 'tbl_products_shipped_by_seller';
	const DB_PRODUCT_SHIPPED_BY_SELLER_PREFIX = 'psbs_';
	
	const PRODUCT_TYPE_PHYSICAL = 1;
	const PRODUCT_TYPE_DIGITAL = 2;
	
	CONST APPROVED = 1;
	CONST UNAPPROVED = 0;
	
	CONST INVENTORY_TRACK = 1;
	CONST INVENTORY_NOT_TRACK = 0;
	
	CONST CONDITION_NEW = 1;
	CONST CONDITION_USED = 2;
	CONST CONDITION_REFURBISH = 3;
	
	CONST PRODUCT_VIEW_ORGINAL_URL ='products/view/';
	CONST PRODUCT_REVIEWS_ORGINAL_URL ='reviews/product/';
	CONST PRODUCT_MORE_SELLERS_ORGINAL_URL ='products/sellers/';

	public function __construct($id = 0) {
		parent::__construct ( static::DB_TBL, static::DB_TBL_PREFIX . 'id', $id );
	}

	public static function getSearchObject($langId = 0 , $isDeleted = true) {
		$srch = new SearchBase(static::DB_TBL, 'tp');

		if ( $langId > 0) {
			$srch->joinTable( static::DB_LANG_TBL, 'LEFT OUTER JOIN',
			'productlang_product_id = tp.product_id
			AND productlang_lang_id = ' . $langId, 'tp_l');
		}

		if($isDeleted){
			$srch->addCondition(static::DB_TBL_PREFIX . 'deleted','=',applicationConstants::NO);
		}
		
		$srch->addOrder( static::DB_TBL_PREFIX . 'active', 'DESC' );
		return $srch;
	}
	
	public static function getApproveUnApproveArr($langId){
		$langId = FatUtility::int($langId);
		if($langId < 1){
			$langId = FatApp::getConfig('CONF_ADMIN_DEFAULT_LANG');
		}
		
		return array(
			static::UNAPPROVED => Labels::getLabel('LBL_Un-Approved',$langId),
			static::APPROVED => Labels::getLabel('LBL_Approved',$langId),
		);
	}

	public static function getInventoryTrackArr($langId){
		$langId = FatUtility::int($langId);
		if($langId < 1){
			$langId = FatApp::getConfig('CONF_ADMIN_DEFAULT_LANG');
		}
		
		return array(
			static::INVENTORY_TRACK => Labels::getLabel('LBL_Track',$langId),
			static::INVENTORY_NOT_TRACK => Labels::getLabel('LBL_Do_not_track',$langId)
		);
	}
	
	public static function getConditionArr($langId){
		$langId = FatUtility::int($langId);
		if($langId < 1){
			$langId = FatApp::getConfig('CONF_ADMIN_DEFAULT_LANG');
		}
		
		return array(
			static::CONDITION_NEW => Labels::getLabel('LBL_New',$langId),
			static::CONDITION_USED => Labels::getLabel('LBL_Used',$langId),
			static::CONDITION_REFURBISH => Labels::getLabel('LBL_Refurbished',$langId)
		);
	}
	
	public static function getProductTypes($langId = 0){
		$langId = FatUtility::convertToType($langId, FatUtility::VAR_INT);
		if( !$langId ){
			trigger_error(Labels::getLabel("ERR_Arguments_not_specified.",$this->commonLangId), E_USER_ERROR);
			return false;
		}
		return array(
			self::PRODUCT_TYPE_PHYSICAL => Labels::getLabel('LBL_Physical', $langId),
			self::PRODUCT_TYPE_DIGITAL => Labels::getLabel('LBL_Digital', $langId)
		);
	}
	
	public static function getAttributesById( $recordId, $attr = null) {
		$row = parent::getAttributesById( $recordId, $attr );
			
		/* get Numeric attributes data[ */
		if( !$attr ){
			$num_attr_row = static::getProductNumericAttributes($recordId);
			if( !empty($num_attr_row) ){
				$row = array_merge( $row, $num_attr_row );
			}
		}
		/* ] */
		return $row;
	}
	
	public static function getProductDataById($langId = 0, $productId = 0, $attr =  array()){
		$srch  = self::getSearchObject($langId);
		$srch->addCondition('product_id','=',$productId);
		 $srch->doNotLimitRecords(true);
        $srch->doNotCalculateRecords(true);
		if ( null != $attr ) {
			if (is_array($attr)) {
				$srch->addMultipleFields($attr);
			}
			elseif (is_string($attr)) {
				$srch->addFld($attr);
			}
		}
       	$rs = $srch->getResultSet();
		
        $row = FatApp::getDb()->fetch($rs);
        if($row==false) return array();
        else return $row;
		
	}
	
	public function deleteProductImage( $product_id, $image_id ){
		$product_id = FatUtility :: int($product_id);
		$image_id = FatUtility :: int($image_id);
		if( $product_id < 1 || $image_id < 1 ){
			$this->error = Labels::getLabel('ERR_Invalid_Request',$this->commonLangId);
			return false;
		}
		
		$fileHandlerObj = new AttachedFile();
		if( !$fileHandlerObj->deleteFile( AttachedFile::FILETYPE_PRODUCT_IMAGE, $product_id, $image_id )){
			$this->error = $fileHandlerObj->getError();
			return false;
		}
		return true;
	}

	public function updateProdImagesOrder( $product_id, $order){
		$product_id = FatUtility :: int($product_id);
		if(is_array($order) && sizeof($order) > 0){
			foreach($order as $i => $id){
				if(FatUtility::int($id) < 1){
					continue;
				}
				FatApp::getDb()->updateFromArray('tbl_attached_files',array('afile_display_order' => $i),array('smt' => 'afile_type = ? AND afile_record_id = ? AND afile_id = ?','vals' => array(AttachedFile::FILETYPE_PRODUCT_IMAGE, $product_id, $id)));
			}
			return true;
		}
		return false;
	}

	public function addUpdateProductCategories( $product_id, $categories = array() ){
		if( !$product_id ){
			$this->error = Labels::getLabel('ERR_Invalid_Request',$this->commonLangId);
			return false;
		}
		
		FatApp::getDb()->deleteRecords( static::DB_TBL_PRODUCT_TO_CATEGORY, array('smt'=> static::DB_TBL_PRODUCT_TO_CATEGORY_PREFIX.'product_id = ?','vals' => array($product_id) ) );
		if( empty($categories) ){
			return true;
		}

		$record = new TableRecord( static::DB_TBL_PRODUCT_TO_CATEGORY );
		foreach( $categories as $category_id){
			$to_save_arr = array();
			$to_save_arr['ptc_product_id'] = $product_id;
			$to_save_arr['ptc_prodcat_id'] = $category_id;
			$record->assignValues($to_save_arr);
			if(!$record->addNew( array(),$to_save_arr) ){
				$this->error = $record->getError();
				return false;
			}
		}
		return true;
	}

	public function addUpdateProductOption( $product_id, $option_id ){
		$product_id = FatUtility::int($product_id);
		$option_id = FatUtility::int($option_id);
		if( !$product_id || !$option_id ){
			$this->error = Labels::getLabel('ERR_Invalid_Request',$this->commonLangId);;
			return false;
		}
		$record = new TableRecord( static::DB_PRODUCT_TO_OPTION );
		$to_save_arr = array();
		$to_save_arr[static::DB_PRODUCT_TO_OPTION_PREFIX.'product_id'] = $product_id;
		$to_save_arr[static::DB_PRODUCT_TO_OPTION_PREFIX.'option_id'] = $option_id;
		$record->assignValues($to_save_arr);
		if(!$record->addNew( array(),$to_save_arr) ){
			$this->error = $record->getError();
			return false;
		}
		return true; 
	}
	
	public function removeProductOption( $product_id, $option_id ){
		$db = FatApp::getDb();
		$product_id = FatUtility::int($product_id);
		$option_id = FatUtility::int($option_id);
		if( !$product_id || !$option_id ){
			$this->error = Labels::getLabel('ERR_Invalid_Request',$this->commonLangId);;
			return false;
		}
		if(!$db->deleteRecords( static::DB_PRODUCT_TO_OPTION , array('smt'=> static::DB_PRODUCT_TO_OPTION_PREFIX.'product_id = ? AND '.static::DB_PRODUCT_TO_OPTION_PREFIX . 'option_id = ?','vals' => array($product_id, $option_id) ) )){
			$this->error = $db->getError();
			return false;
		}
		return true;
	}

	public function addUpdateProductTag( $product_id, $tag_id ){
		$product_id = FatUtility::int($product_id);
		$tag_id = FatUtility::int($tag_id);
		if( !$product_id || !$tag_id ){
			$this->error = Labels::getLabel('ERR_Invalid_Request',$this->commonLangId);;
			return false;
		}
		$record = new TableRecord( static::DB_PRODUCT_TO_TAG );
		$to_save_arr = array();
		$to_save_arr[static::DB_PRODUCT_TO_TAG_PREFIX.'product_id'] = $product_id;
		$to_save_arr[static::DB_PRODUCT_TO_TAG_PREFIX.'tag_id'] = $tag_id;
		$record->assignValues($to_save_arr);
		if(!$record->addNew( array(),$to_save_arr) ){
			$this->error = $record->getError();
			return false;
		}
		return true; 
	}
	
	public function addUpdateProductTags( $product_id, $tags = array() ){
		
		if( !$product_id ){
			$this->error = Labels::getLabel('ERR_Invalid_Request',$this->commonLangId);
			return false;
		}
		
		FatApp::getDb()->deleteRecords( static::DB_PRODUCT_TO_TAG, array('smt'=> static::DB_PRODUCT_TO_TAG_PREFIX.'product_id = ?','vals' => array($product_id) ) );
		if( empty($tags) ){
			return true;
		}

		$record = new TableRecord( static::DB_PRODUCT_TO_TAG );
		foreach( $tags as $tag_id){
			$to_save_arr = array();
			$to_save_arr['ptt_product_id'] = $product_id;
			$to_save_arr['ptt_tag_id'] = $tag_id; 
			$record->assignValues($to_save_arr);
			if(!$record->addNew( array(),$to_save_arr) ){
				$this->error = $record->getError();
				return false;
			}
		}
		return true;
	}
	
	public function removeProductTag( $product_id, $tag_id ){
		$db = FatApp::getDb();
		$product_id = FatUtility::int($product_id);
		$tag_id = FatUtility::int($tag_id);
		if( !$product_id || !$tag_id ){
			$this->error = Labels::getLabel('ERR_Invalid_Request',$this->commonLangId);;
			return false;
		}
		if(!$db->deleteRecords( static::DB_PRODUCT_TO_TAG , array('smt'=> static::DB_PRODUCT_TO_TAG_PREFIX.'product_id = ? AND '.static::DB_PRODUCT_TO_TAG_PREFIX . 'tag_id = ?','vals' => array($product_id, $tag_id) ) )){
			$this->error = $db->getError();
			return false;
		}
		return true;
	}
	
	static function getProductShippingRates($product_id,$lang_id,$country_id  =0, $sellerId = 0, $limit=0 ){
		
		$product_id = FatUtility::convertToType($product_id, FatUtility::VAR_INT);
		$lang_id = FatUtility::convertToType($lang_id, FatUtility::VAR_INT);
		if( !$product_id || !$lang_id ){
			//trigger_error(Labels::getLabel("ERR_Arguments_not_specified.",$this->commonLangId), E_USER_ERROR);
			return false;
		}
		$srch = new SearchBase(static::DB_PRODUCT_TO_SHIP, 'tpsr');	
		$srch->joinTable(Countries::DB_TBL_LANG, 'LEFT JOIN', 'tpsr.'.static::DB_PRODUCT_TO_SHIP_PREFIX.'country=tc.'.Countries::DB_TBL_LANG_PREFIX.'country_id and tc.'.Countries::DB_TBL_LANG_PREFIX.'lang_id='.$lang_id, 'tc');
		$srch->joinTable(ShippingCompanies::DB_TBL, 'LEFT JOIN', 'tpsr.pship_company=sc.scompany_id ' , 'sc');
		$srch->joinTable(ShippingCompanies::DB_LANG_TBL, 'LEFT JOIN', 'tpsr.pship_company=tsc.scompanylang_scompany_id and tsc.'.ShippingCompanies::DB_LANG_TBL_PREFIX.'lang_id='.$lang_id , 'tsc');
		$srch->joinTable(ShippingDurations::DB_TBL_LANG, 'LEFT JOIN', 'tpsr.pship_duration=tsd.sdurationlang_sduration_id  and tsd.'.ShippingDurations::DB_TBL_PREFIX_LANG.'lang_id='.$lang_id, 'tsd');
		$srch->joinTable(ShippingDurations::DB_TBL, 'LEFT JOIN', 'tpsr.pship_duration=ts.sduration_id and sduration_deleted =0 ', 'ts');
		$srch->addCondition('tpsr.'.static::DB_PRODUCT_TO_SHIP_PREFIX.'prod_id', '=', intval($product_id));
		if($country_id>0){
			$srch->addDirectCondition('( tpsr.'.static::DB_PRODUCT_TO_SHIP_PREFIX.'country ='. intval($country_id) .' OR '.'tpsr.'.static::DB_PRODUCT_TO_SHIP_PREFIX.'country =-1 )' );
		}
		$srch ->addCondition('tpsr.'.static::DB_PRODUCT_TO_SHIP_PREFIX.'user_id', '=',$sellerId);
		
		$srch->addOrder('(tpsr.'.static::DB_PRODUCT_TO_SHIP_PREFIX.'country = -1),country_name');
		$srch->addMultipleFields(
									array(
										static::DB_PRODUCT_TO_SHIP_PREFIX.'id',
										static::DB_PRODUCT_TO_SHIP_PREFIX.'country',
										static::DB_PRODUCT_TO_SHIP_PREFIX.'user_id',
										static::DB_PRODUCT_TO_SHIP_PREFIX.'company',
										static::DB_PRODUCT_TO_SHIP_PREFIX.'duration',
										static::DB_PRODUCT_TO_SHIP_PREFIX.'charges',
										static::DB_PRODUCT_TO_SHIP_PREFIX.'additional_charges',
										'IFNULL('.Countries::DB_TBL_PREFIX.'name','\''.Labels::getLabel('LBL_Everywhere_Else',$lang_id).'\') as country_name',
										'ifNull('.ShippingCompanies::DB_TBL_PREFIX.'name',ShippingCompanies::DB_TBL_PREFIX.'identifier) as '.ShippingCompanies::DB_TBL_PREFIX.'name',
										ShippingCompanies::DB_TBL_PREFIX.'id',
										ShippingCompanies::DB_LANG_TBL_PREFIX.'scompany_id',
										ShippingDurations::DB_TBL_PREFIX.'name',
										ShippingDurations::DB_TBL_PREFIX.'id',
										ShippingDurations::DB_TBL_PREFIX.'from',
										ShippingDurations::DB_TBL_PREFIX.'identifier ',
										ShippingDurations::DB_TBL_PREFIX.'to',
										ShippingDurations::DB_TBL_PREFIX.'days_or_weeks',
										
										));
		

        if($limit > 0){
			$srch->setPageSize($limit);
		}else{
			$srch->doNotLimitRecords(true);
			$srch->doNotCalculateRecords(true);
		}
       	$rs = $srch->getResultSet();
		/* echo $srch->getQuery();die; */
        $row = FatApp::getDb()->fetchAll($rs);
		
        if($row==false) return array();
        else return $row;
	}
	
	static function getProductFreeShippingAvailabilty($product_id,$lang_id,$country_id  =0, $sellerId = 0 ){
		
		$product_id = FatUtility::convertToType($product_id, FatUtility::VAR_INT);
		$lang_id = FatUtility::convertToType($lang_id, FatUtility::VAR_INT);
		if( !$product_id || !$lang_id ){
			//trigger_error(Labels::getLabel("ERR_Arguments_not_specified.",$this->commonLangId), E_USER_ERROR);
			return false;
		}
		$srch = new SearchBase(static::DB_TBL_PRODUCT_SHIPPING, 'tps');	
		$srch->joinTable(Countries::DB_TBL_LANG, 'LEFT JOIN', 'tps.'.static::DB_TBL_PRODUCT_SHIPPING_PREFIX.'from_country_id=tc.'.Countries::DB_TBL_LANG_PREFIX.'country_id and tc.'.Countries::DB_TBL_LANG_PREFIX.'lang_id='.$lang_id, 'tc');
		$srch->addCondition('tps.'.static::DB_TBL_PRODUCT_SHIPPING_PREFIX.'product_id', '=', intval($product_id));
		
		$srch ->addCondition('tps.'.static::DB_TBL_PRODUCT_SHIPPING_PREFIX.'user_id', '=',$sellerId);
		$srch->addFld(
									array(
									
										static::DB_TBL_PRODUCT_SHIPPING_PREFIX.'free',
									
										
										));
		

        $srch->doNotLimitRecords(true);
        $srch->doNotCalculateRecords(true);
       	$rs = $srch->getResultSet();
	
        $row = FatApp::getDb()->fetch($rs);
	
		if($row){
			return $row[static::DB_TBL_PRODUCT_SHIPPING_PREFIX.'free'];
		}
        return 0;
	}
	
	public static function getProductShippingDetails($productId,$langId , $userId =0){
		$productId = FatUtility::convertToType($productId, FatUtility::VAR_INT);
		if( !$productId || !$langId ){
			trigger_error(Labels::getLabel("ERR_Arguments_not_specified.",CommonHelper::getLangId()), E_USER_ERROR);
			return false;
		}
		$srch = new SearchBase( static::DB_TBL_PRODUCT_SHIPPING );
		$srch->addCondition( static::DB_TBL_PRODUCT_SHIPPING_PREFIX . 'product_id', '=', $productId );
		$srch->addCondition( static::DB_TBL_PRODUCT_SHIPPING_PREFIX . 'user_id', '=', $userId );
		
		$rs = $srch->getResultSet();
		$db = FatApp::getDb();
		$row = $db->fetch($rs);
		return $row;
	}
	
	public static function getProductOptions( $product_id, $lang_id, $includeOptionValues = false, $option_is_separate_images = 0 ){
		$product_id = FatUtility::convertToType($product_id, FatUtility::VAR_INT);
		$lang_id = FatUtility::convertToType($lang_id, FatUtility::VAR_INT);
		if( !$product_id || !$lang_id ){
			trigger_error(Labels::getLabel("ERR_Arguments_not_specified.",CommonHelper::getLangId()), E_USER_ERROR);
			return false;
		}
		
		$srch = new SearchBase( static::DB_PRODUCT_TO_OPTION );
		$srch->addCondition( static::DB_PRODUCT_TO_OPTION_PREFIX . 'product_id', '=', $product_id );
		$srch->joinTable( Option::DB_TBL, 'INNER JOIN', Option::DB_TBL_PREFIX.'id = '.static::DB_PRODUCT_TO_OPTION_PREFIX.'option_id');

		$srch->joinTable( Option::DB_TBL.'_lang', 'LEFT JOIN', 'lang.optionlang_option_id = ' . Option::DB_TBL_PREFIX.'id AND optionlang_lang_id = '.$lang_id, 'lang');
		
		$srch->addMultipleFields(array('option_id','option_name','option_identifier'));
		
		if( $option_is_separate_images ){
			$srch->addCondition( 'option_is_separate_images', '=', applicationConstants::YES );
		}
		
		$rs = $srch->getResultSet();
		$db = FatApp::getDb();
		$data = array();
		while( $row = $db->fetch($rs) ){
			if($includeOptionValues){
				$row['optionValues'] = static::getOptionValues($row['option_id'], $lang_id);
			}
			$data[] = $row;
		}
		return $data;
	}
	
	public static function getProductSpecifications( $product_id, $lang_id ){
		$product_id = FatUtility::convertToType($product_id, FatUtility::VAR_INT);
		$lang_id = FatUtility::convertToType($lang_id, FatUtility::VAR_INT);
		if( !$product_id || !$lang_id ){
			trigger_error(Labels::getLabel("ERR_Arguments_not_specified.",CommonHelper::getLangId()), E_USER_ERROR);
			return false;
		}
		$data = array();
		$languages = Language::getAllNames();
		foreach($languages as $langId => $langName){
			$srch = new SearchBase( static::DB_PRODUCT_SPECIFICATION );
			$srch->addCondition( static::DB_PRODUCT_SPECIFICATION_PREFIX . 'product_id', '=', $product_id );
			$srch->joinTable( static::DB_PRODUCT_LANG_SPECIFICATION, 'LEFT JOIN', static::DB_PRODUCT_SPECIFICATION_PREFIX.'id = '.static::DB_PRODUCT_LANG_SPECIFICATION_PREFIX.'prodspec_id and '.static::DB_PRODUCT_LANG_SPECIFICATION_PREFIX.'lang_id ='.$langId );

			
			
			$srch->addMultipleFields(array(
									static::DB_PRODUCT_SPECIFICATION_PREFIX.'id',
									static::DB_PRODUCT_SPECIFICATION_PREFIX.'name',
									static::DB_PRODUCT_SPECIFICATION_PREFIX.'value'
									
									)
								);				
			$rs = $srch->getResultSet();
			$row = FatApp::getDb()->fetchAll($rs);
			foreach($row as $resRow)
			$data[$resRow[static::DB_PRODUCT_SPECIFICATION_PREFIX.'id']][$langId]=$resRow ;
			
			
		}
		
		return $data;
	}
	
	public static function getProductTags( $product_id, $lang_id = 0 ){
		$product_id = FatUtility::convertToType($product_id, FatUtility::VAR_INT);
		$lang_id = FatUtility::convertToType($lang_id, FatUtility::VAR_INT);
		if( !$product_id ){
			trigger_error(Labels::getLabel("ERR_Arguments_not_specified.",$this->commonLangId), E_USER_ERROR);
			return false;
		}
		
		$srch = new SearchBase( static::DB_PRODUCT_TO_TAG );
		$srch->addCondition( static::DB_PRODUCT_TO_TAG_PREFIX . 'product_id', '=', $product_id );
		$srch->joinTable( Tag::DB_TBL, 'INNER JOIN', Tag::DB_TBL_PREFIX.'id = '.static::DB_PRODUCT_TO_TAG_PREFIX.'tag_id');

		$srch->addMultipleFields( array('tag_id', 'tag_identifier') );
		
		if( $lang_id ){
			$srch->joinTable( Tag::DB_TBL.'_lang', 'LEFT JOIN', 'lang.taglang_tag_id = ' . Tag::DB_TBL_PREFIX.'id AND taglang_lang_id = '.$lang_id, 'lang');
			$srch->addFld('tag_name');
		}
		
		$rs = $srch->getResultSet();
		$db = FatApp::getDb();
		$data = array();
		while( $row = $db->fetch($rs) ){
			$data[] = $row;
		}
		return $data;
	}
	
	public static function getProductIdsByTagId( $tagId ){
		$tagId = FatUtility::int( $tagId );
		if( !$tagId ){
			return array();
		}
		
		$srch = new SearchBase( static::DB_PRODUCT_TO_TAG );
		$srch->doNotCalculateRecords();
		$srch->doNotLimitRecords();
		$srch->addCondition( static::DB_PRODUCT_TO_TAG_PREFIX.'tag_id', '=', $tagId );
		$rs = $srch->getResultSet();
		return FatApp::getDb()->fetchAll( $rs );
	}
	
	public static function getOptionValues( $option_id, $lang_id ){
		$option_id = FatUtility::int($option_id);
		$lang_id = FatUtility::int($lang_id);
		if( !$option_id || !$lang_id ){
			trigger_error( Labels::getLabel('ERR_Invalid_Arguments!',$this->commonLangId), E_USER_ERROR);
		}
		$srch = new SearchBase( OptionValue::DB_TBL );
		$srch->joinTable( OptionValue::DB_TBL.'_lang', 'LEFT JOIN', 'lang.optionvaluelang_optionvalue_id = ' . OptionValue::DB_TBL_PREFIX.'id AND optionvaluelang_lang_id = '.$lang_id, 'lang');
		$srch->addCondition( OptionValue::DB_TBL_PREFIX.'option_id', '=', $option_id );
		$srch->doNotCalculateRecords();
		$srch->doNotLimitRecords();
		$srch->addOrder('optionvalue_display_order');
		$srch->addMultipleFields(array('optionvalue_id', 'optionvalue_name' ));
		$rs = $srch->getResultSet();
		$db = FatApp::getDb();
		return $db->fetchAllAssoc($rs);
	}

	public function getProductCategories( $product_id ){
		$srch = new SearchBase( static::DB_TBL_PRODUCT_TO_CATEGORY, 'ptc' );
		$srch->addCondition( static::DB_TBL_PRODUCT_TO_CATEGORY_PREFIX . 'product_id', '=', $product_id );

		$srch->joinTable( ProductCategory::DB_TBL, 'INNER JOIN', ProductCategory::DB_TBL_PREFIX.'id = ptc.'.static::DB_TBL_PRODUCT_TO_CATEGORY_PREFIX.'prodcat_id','cat');
		$srch->addMultipleFields(array('prodcat_id'));
		$rs = $srch->getResultSet();
		$records = FatApp::getDb()->fetchAll($rs,'prodcat_id');
		if( !$records ){ return false; }
		return $records;
	}
	
	public function addUpdateNumericAttributes( $data ){
		$record = new TableRecord( self::DB_NUMERIC_ATTRIBUTES_TBL );
		$record->assignValues( $data );
		if( !$record->addNew(array(),$data) ){
			$this->error = $record->getError();
			return false;
		}
		return true;
	}
	
	public function addUpdateTextualAttributes( $data ){
		$record = new TableRecord( self::DB_TEXT_ATTRIBUTES_TBL );
		$record->assignValues( $data );
		if(!$record->addNew(array(),$data)){
			$this->error = $record->getError();
			return false;
		}
		return true;
	}
	
	public static function getProductNumericAttributes( $product_id ){
		if( !$product_id ){
			trigger_error(Labels::getLabel('ERR_Invalid_Arguments!',CommonHelper::getLangId()), E_USER_ERROR);
		}
		$record = new TableRecord( static::DB_NUMERIC_ATTRIBUTES_TBL );
		$record->loadFromDb(array('smt' => static::DB_NUMERIC_ATTRIBUTES_PREFIX . 'product_id = ?', 'vals' => array($product_id)));
		return $record->getFlds();
	}
	
	public static function getProductTextualAttributes( $langId, $product_id ){
		$product_id = FatUtility::int($product_id);
		$langId = FatUtility::int($langId);
		if( !$product_id || !$langId ){
			trigger_error(Labels::getLabel('ERR_Invalid_Arguments!',$this->commonLangId), E_USER_ERROR);
		}
		$record = new TableRecord( static::DB_TEXT_ATTRIBUTES_TBL );
		$record->loadFromDb(array('smt' => static::DB_TEXT_ATTRIBUTES_PREFIX . 'product_id = ? AND ' . static::DB_TEXT_ATTRIBUTES_PREFIX . 'lang_id = ?', 'vals' => array($product_id, $langId)));
		return $record->getFlds();
	}
	
	public static function generateProductOptionsUrl( $selprod_id, $selectedOptions, $option_id, $optionvalue_id, $product_id){
		
		$selectedOptions[$option_id] = $optionvalue_id;
		sort($selectedOptions);
		
		$selprod_code = $product_id.'_'.implode('_', $selectedOptions);
		
		$prodSrchObj = new ProductSearch( );
		$prodSrchObj->setDefinedCriteria();
		$prodSrchObj->joinProductToCategory();
		$prodSrchObj->doNotCalculateRecords();
		$prodSrchObj->addCondition( 'selprod_id', '!=', $selprod_id );
		$prodSrchObj->addMultipleFields(array('product_id','selprod_id','theprice'));
		$prodSrchObj->addCondition('product_id','=',$product_id);
		
		$prodSrch = clone $prodSrchObj;
		
		$prodSrch->addCondition('selprod_code','=',$selprod_code);
		$prodSrch->doNotLimitRecords();
		$prodSrch->addOrder('theprice', 'ASC');
		$productRs = $prodSrch->getResultSet();
		//echo $prodSrch->getQuery();
		$product = FatApp::getDb()->fetch($productRs);
		if( $product ){
			return CommonHelper::generateUrl('Products','view', array($product['selprod_id']));
		} else {
			/* $prodSrch2 =  new ProductSearch( CommonHelper::getLangId() );
			$prodSrch2->doNotCalculateRecords();
			$prodSrch2->joinSellerProducts();
			$prodSrch2->joinSellers();
			$prodSrch2->joinShops();
			$prodSrch2->joinShopCountry();
			$prodSrch2->joinShopState();
			$prodSrch2->joinBrands();
			$prodSrch2->addCondition( 'selprod_id', '!=', $selprod_id );
			$prodSrch2->addCondition('product_id','=',$product_id);
			$prodSrch2->addCondition('selprod_code','LIKE', '%_'.$optionvalue_id.'%');
			$prodSrch2->addMultipleFields(array('selprod_id','selprod_code','selprod_user_id','theprice'));
			$prodSrch2->setPageSize(1);
			$productRs = $prodSrch2->getResultSet();
			$product = FatApp::getDb()->fetch($productRs); */
			
			$prodSrch2 =  new ProductSearch( CommonHelper::getLangId() );
			$prodSrch2->doNotCalculateRecords();
			$prodSrch2->setDefinedCriteria();
			$prodSrch2->addCondition( 'selprod_id', '!=', $selprod_id );
			$prodSrch2->addCondition('product_id','=',$product_id);
			$prodSrch2->addCondition('selprod_code','LIKE', '%_'.$optionvalue_id.'%');
			$prodSrch2->addMultipleFields(array('selprod_id', 'special_price_found', 'theprice'));
			$prodSrch2->setPageSize(1);
			$prodSrch2->addOrder('theprice', 'ASC');
			$productRs = $prodSrch2->getResultSet();
			$product = FatApp::getDb()->fetch($productRs);
			
			if( $product ){
				return CommonHelper::generateUrl('Products','view', array($product['selprod_id']))."::";
			} else {
				return false;
			}
			return false;
		}
	}
	
	public static function uniqueProductAction($selprodCode,$weightageKey){
		/* $ipAddress = $_SERVER['REMOTE_ADDR'];
		list($product_id) = explode('_',$selprodCode);
		$product_id = FatUtility::int($product_id);
		
		$srch = new SearchBase('tbl_smart_log_actions');
		
		$date = date('Y-m-d H:i:s');
		$currentDate = strtotime($date);
		$futureDate = $currentDate - (60*5);
		$formatDate = date("Y-m-d H:i:s", $futureDate);

		$srch->addDirectCondition("slog_ip = '".$ipAddress."' and '".$formatDate."' < slog_datetime and  	slog_swsetting_key = '".$weightageKey."' and slog_record_code = '".$selprodCode."' and slog_record_id = '".$product_id."' and slog_type = '".SmartUserActivityBrowsing::TYPE_PRODUCT."'");
		$srch->doNotCalculateRecords();
		$srch->doNotLimitRecords();
		$srch->addMultipleFields(array('slog_ip'));
		$rs = $srch->getResultSet();
		$row =  FatApp::getDb()->fetch($rs);		
		return ($row == false)?true:false; */
	}
	
	public static function recordProductWeightage($selprodCode,$weightageKey,$eventWeightage = 0){
		list($productId) = explode('_',$selprodCode);
		$productId = FatUtility::int($productId);
				
		if(1 > $productId) { return false;}
		
		if ($eventWeightage == 0){
			$weightageArr = SmartWeightageSettings::getWeightageAssoc();
			$eventWeightage = !empty($weightageArr[$weightageKey]) ? $weightageArr[$weightageKey] : 0;
		}
		
		if (!UserAuthentication::isUserLogged()) {
			$userId = CommonHelper::getUserIdFromCookies();
		}else{
			$userId = UserAuthentication::getLoggedUserId();
		}
		
		$record = new TableRecord('tbl_recommendation_activity_browsing');
		
		$assignFields = array();
		$assignFields['rab_session_id'] = session_id();
		$assignFields['rab_user_id'] = $userId;
		$assignFields['rab_record_id'] = $productId;
		$assignFields['rab_record_type'] = SmartUserActivityBrowsing::TYPE_PRODUCT;
		$assignFields['rab_weightage_key'] = $weightageKey;
		$assignFields['rab_weightage'] = $eventWeightage;
		$assignFields['rab_last_action_datetime'] = date('Y-m-d H:i:s');
		
		$onDuplicateKeyUpdate = array_merge($assignFields,array('rab_weightage'=>'mysql_func_rab_weightage + '.$eventWeightage));
		
		FatApp::getDb()->insertFromArray('tbl_recommendation_activity_browsing',$assignFields,true,array(),$onDuplicateKeyUpdate); 
		
	}
	
	public static function addUpdateProductBrowsingHistory($selprodCode,$weightageKey,$weightageVal = 1){
		/* list($productId) = explode('_',$selprodCode);
		$productId = FatUtility::int($productId);
		$weightageVal = FatUtility::int($weightageVal);
			
		$weightageKey = FatUtility::int($weightageKey);
		$weightageKey = 1 ;
		
		if(1 > $weightageKey || 1 > $weightageVal) { return false;}
		
		if(!static::uniqueProductAction($selprodCode,$weightageKey)){ return false ;}
		
		if (!UserAuthentication::isUserLogged()) {
			$userId = CommonHelper::getUserIdFromCookies();
		}else{
			$userId = UserAuthentication::getLoggedUserId();
		}
		
		$record = new TableRecord('tbl_products_browsing_history');
			
		$assignFields = array();
		$assignFields['pbhistory_sessionid'] = session_id();
		$assignFields['pbhistory_selprod_code'] = $selprodCode;
		$assignFields['pbhistory_swsetting_key'] = $weightageKey;
		$assignFields['pbhistory_user_id'] = $userId;
		$assignFields['pbhistory_product_id'] = $productId;
		$assignFields['pbhistory_count'] = $weightageVal;
		$assignFields['pbhistory_datetime'] = date('Y-m-d H:i:s');
		
		$onDuplicateKeyUpdate = array_merge($assignFields,array('pbhistory_count'=>'mysql_func_pbhistory_count + '.$weightageVal));
		
		FatApp::getDb()->insertFromArray('tbl_products_browsing_history',$assignFields,true,array(),$onDuplicateKeyUpdate);  */
		
	}
	
	public static function tempHoldStockCount($selprod_id = 0,$userId = 0,$pshold_prodgroup_id = 0,$useProductGroup = false){
		$selprod_id = FatUtility::int($selprod_id);
		$pshold_prodgroup_id = FatUtility::int($pshold_prodgroup_id);
		$intervalInMinutes = FatApp::getConfig( 'cart_stock_hold_minutes', FatUtility::VAR_INT, 15 );
		
		$srch = new SearchBase('tbl_product_stock_hold');
		$srch->doNotCalculateRecords();
		$srch->addOrder('pshold_id', 'ASC');
		$srch->addCondition( 'pshold_added_on', '>=', 'mysql_func_DATE_SUB( NOW(), INTERVAL ' . $intervalInMinutes . ' MINUTE )', 'AND', true );
		$srch->addCondition( 'pshold_selprod_id', '=', $selprod_id);
		
		if($useProductGroup == true){
			$srch->addCondition( 'pshold_prodgroup_id', '=', $pshold_prodgroup_id);
		}
		
		if($userId > 0){
			$srch->addCondition( 'pshold_user_id', '=', $userId);
		}		
		$srch->addMultipleFields(array('sum(pshold_selprod_stock) as stockHold'));
		$srch->setPageNumber(1);
		$srch->setPageSize(1);
		$rs = $srch->getResultSet();
		$stockHoldRow = FatApp::getDb()->fetch($rs);
		if($stockHoldRow == false){
			return 0;
		}
		return $stockHoldRow['stockHold'];
	}
	
	public function getRecommendedProducts(){
		$productId = FatUtility::int($this->mainTableRecordId);
		if($productId <= 0)
		{
			
		}
		
	}
	
	public function addUpdateUserFavoriteProduct( $user_id, $product_id ){
		$user_id = FatUtility::int( $user_id );
		$product_id = FatUtility::int( $product_id );
		
		$data_to_save = array( 'ufp_user_id' => $user_id, 'ufp_product_id' => $product_id );
		$data_to_save_on_duplicate = array( 'ufp_product_id' => $product_id );
		if( !FatApp::getDb()->insertFromArray( static::DB_TBL_PRODUCT_FAVORITE, $data_to_save, false, array(), $data_to_save_on_duplicate ) ){
			$this->error = FatApp::getDb()->getError(); 
			return false;
		}
		return true;
	}
	
	public static function getUserFavouriteProducts($user_id,$langId){
		$user_id = FatUtility::int( $user_id );
		$srch = new UserFavoriteProductSearch();
		$srch->setDefinedCriteria( $langId);
		$srch->joinBrands();
		$srch->joinSellers();
		$srch->joinShops();
		$srch->joinProductToCategory();
		$srch->joinSellerSubscription($langId ,true);
		$srch->addSubscriptionValidCondition();
		$srch->addCondition('selprod_deleted','=',applicationConstants::NO);
		$srch->addCondition('ufp_user_id', '=', $user_id );
		$srch->addMultipleFields(array( 'selprod_id', 'IFNULL(selprod_title  ,IFNULL(product_name, product_identifier)) as selprod_title', 'product_id', 'IFNULL(product_name, product_identifier) as product_name', 'IF(selprod_stock > 0, 1, 0) AS in_stock'));
		$srch->setPageNumber(1);
		$srch->setPageSize(4);
		$srch->addGroupBy('selprod_id');
		
		/* die($srch->getQuery());  */
		$rs = $srch->getResultSet();
		$result['uwlist_id'] = 0;
		$result['uwlist_title'] = Labels::getLabel('LBL_Products_That_I_Love',$langId);
		$result['uwlist_type'] = UserWishList::TYPE_FAVOURITE;
		
		$result['totalProducts'] = $srch->recordCount();
		$result['products'] = FatApp::getDb()->fetchAll($rs);
		return $result;
	}
	
	public static function getProductMetaData($selProductId = 0){
		if( $selProductId <= 0 ){
			return false;
		}
		$srch = MetaTag::getSearchObject();
		$srch->addCondition( MetaTag::DB_TBL_PREFIX.'record_id','=',$selProductId);
		$srch->addMultipleFields(array('meta_id','meta_identifier'));
		$rs = $srch->getResultSet();
		$records = FatApp::getDb()->fetch($rs);
		return $records;
	}
	
	public static function isProductShippedBySeller($productId,$productAddedBySellerId,$selProdSellerId){
		$productId = FatUtility::int($productId);	
		$productAddedBySellerId = FatUtility::int($productAddedBySellerId);	
		$selProdSellerId = FatUtility::int($selProdSellerId);	
		if($productAddedBySellerId && $productAddedBySellerId == $selProdSellerId){
			return true;
		}
		$srch = new SearchBase(static::DB_PRODUCT_SHIPPED_BY_SELLER, 'psbs');
		$srch->addCondition('psbs_product_id','=',$productId); 
		$srch->addCondition('psbs_user_id','=',$selProdSellerId);
		$srch->doNotCalculateRecords();
		$srch->setPageSize(1);
		$rs = $srch->getResultSet();
		$row = FatApp::getDb()->fetch($rs);
		if(!empty($row) && $row['psbs_user_id'] == $selProdSellerId){
			return true;
		}
		return false;
	}
	
	public function getTotalProductsAddedByUser($user_id){
		$srch = SellerProduct::getSearchObject( CommonHelper::getLangId() );
		$srch->joinTable( Product::DB_TBL, 'INNER JOIN', 'p.product_id = sp.selprod_product_id', 'p' );
		$srch->joinTable( Product::DB_LANG_TBL, 'LEFT OUTER JOIN', 'p.product_id = p_l.productlang_product_id AND p_l.productlang_lang_id = '.CommonHelper::getLangId(), 'p_l' );
		$srch->addOrder('product_name');
		$srch->addCondition('selprod_user_id', '=',$user_id );
		
		$srch->addCondition('selprod_deleted', '=',0);
		$srch->addMultipleFields(array(
			'count(selprod_id) as totProducts'));
		$srch->addOrder('selprod_active', 'DESC');
		
		$db = FatApp::getDb();
		$rs = $srch->getResultSet();
		$produtcCountList = $db->fetch($rs);
		$totalProduct = $produtcCountList['totProducts'];
		return $totalProduct;
	}
	
	public static function getProductShippingTitle($shippingDetails = array(),$langId){
		if(!$shippingDetails){
			return ;
		}else{
			
			
			return FatUtility::decodeHtmlEntities('<em><strong>'.$shippingDetails['country_name'].'</em></strong> '.Labels::getLabel('LBL_by',$langId).' <strong>'.$shippingDetails['scompany_name'].'</strong> '.Labels::getLabel('LBL_in',$langId).' '.ShippingDurations::getShippingDurationTitle($shippingDetails,$langId));
			
			
		}
	}
	
	public static function IsSellProdAvailableForUser( $selProdCode , $langId, $userId= 0,$selprod_id ){
		$userId = FatUtility::int($userId);
		$langId = FatUtility::int($langId);
		if($langId < 1){
			$langId = FatApp::getConfig('CONF_ADMIN_DEFAULT_LANG');
		}
		if(1>$userId){
			return false;
		}else{
			$srch = SellerProduct::getSearchObject($langId);
			$srch->addCondition('selprod_code','=',$selProdCode);
			$srch->addCondition('selprod_user_id','=',$userId);
			$srch->addCondition('selprod_deleted','=',applicationConstants::NO);
			if($selprod_id){
				$srch->addCondition('selprod_id','!=',$selprod_id);
			}
			$db = FatApp::getDb();
			$rs = $srch->getResultSet();
			
			$productCount  =  $srch->recordCount();
			if($productCount>0){
				return false;
			}else{
				return true;
			}
		}
	}

	public static function addUpdateProductSellerShipping( $product_id, $data_to_be_save, $userId ){
		$productSellerShiping = array();
		$productSellerShiping['ps_product_id']= $product_id;
		$productSellerShiping['ps_user_id']= $userId;
		$productSellerShiping['ps_from_country_id']= $data_to_be_save['ps_from_country_id'];
		$productSellerShiping['ps_free']=  $data_to_be_save['ps_free'];
		if(!FatApp::getDb()->insertFromArray(PRODUCT::DB_TBL_PRODUCT_SHIPPING,$productSellerShiping,false,array(),$productSellerShiping)){			
			$this->error = FatApp::getDb()->getError();
			return false;
		}
		return true;
	}
	
	public static function addUpdateProductShippingRates( $product_id, $data, $userId = 0 ){
		static::removeProductShippingRates($product_id,$userId);
		
		if(!empty($data) && count($data)>0){		
			foreach($data as $key=>$val){			
				if(isset($val["country_id"]) && ($val["country_id"]>0 || $val["country_id"]==-1) && $val["company_id"]>0 && $val["processing_time_id"]>0){
					$prodShipData = array(
						'pship_prod_id'=>$product_id,
						'pship_country'=>(isset($val["country_id"]) && FatUtility::int($val["country_id"]))?FatUtility::int($val["country_id"]):0,
						'pship_user_id'=>$userId,
						'pship_company'=>(isset($val["company_id"]) && FatUtility::int($val["company_id"]))?FatUtility::int($val["company_id"]):0,
						'pship_duration'=>(isset($val["processing_time_id"]) && FatUtility::int($val["processing_time_id"]))?FatUtility::int($val["processing_time_id"]):0,
						'pship_charges'=>FatUtility::float($val["cost"]),
						'pship_additional_charges'=>FatUtility::float($val["additional_cost"]),
					); 

					if(!FatApp::getDb()->insertFromArray(ShippingApi::DB_TBL_PRODUCT_SHIPPING_RATES,$prodShipData,false,array(),$prodShipData)){			
						$this->error = FatApp::getDb()->getError();
						return false;
					}
					
				}
			}
		}
		return true;
	}
	
	public static function removeProductShippingRates( $product_id, $userId ){
		$db = FatApp::getDb();
		$product_id = FatUtility::int($product_id);
		$userId = FatUtility::int($userId);
				
		if(!$db->deleteRecords( ShippingApi::DB_TBL_PRODUCT_SHIPPING_RATES , array('smt'=> ShippingApi::DB_TBL_PRODUCT_SHIPPING_RATES_PREFIX.'prod_id = ? and '.ShippingApi::DB_TBL_PRODUCT_SHIPPING_RATES_PREFIX.'user_id = ?','vals' => array($product_id,$userId) ) )){
			$this->error = $db->getError();
			return false;
		}		
		return true;
	}
	
	public function removeProductCategory( $product_id, $option_id ){
		$db = FatApp::getDb();
		$product_id = FatUtility::int($product_id);
		$option_id = FatUtility::int($option_id);
		if( !$product_id || !$option_id ){
			$this->error = Labels::getLabel('ERR_Invalid_Request',$this->commonLangId);;
			return false;
		}
		if(!$db->deleteRecords( static::DB_TBL_PRODUCT_TO_CATEGORY , array('smt'=> static::DB_TBL_PRODUCT_TO_CATEGORY_PREFIX.'product_id = ? AND '.static::DB_TBL_PRODUCT_TO_CATEGORY_PREFIX . 'prodcat_id = ?','vals' => array($product_id, $option_id) ) )){
			$this->error = $db->getError();
			return false;
		}
		return true;
	}
	
	public function addUpdateProductCategory( $product_id, $option_id ){
		$product_id = FatUtility::int($product_id);
		$option_id = FatUtility::int($option_id);
		if( !$product_id || !$option_id ){
			$this->error = Labels::getLabel('ERR_Invalid_Request',$this->commonLangId);;
			return false;
		}
		$record = new TableRecord( static::DB_TBL_PRODUCT_TO_CATEGORY );
		$to_save_arr = array();
		$to_save_arr[static::DB_TBL_PRODUCT_TO_CATEGORY_PREFIX.'product_id'] = $product_id;
		$to_save_arr[static::DB_TBL_PRODUCT_TO_CATEGORY_PREFIX.'prodcat_id'] = $option_id;
		$record->assignValues($to_save_arr);
		if(!$record->addNew( array(),$to_save_arr) ){
			$this->error = $record->getError();
			return false;
		}
		return true; 
	}
	
	public function deleteProduct(){
		$productId = FatUtility::int($this->mainTableRecordId);
		if( 0 >= $productId ){
			Message::addErrorMessage($this->str_invalid_request_id);
			FatUtility::dieWithError( Message::getHtml() );	
		}
		
		$db = FatApp::getDb();
		
		if (! $db->updateFromArray ( static::DB_TBL, array (
				static::DB_TBL_PREFIX . 'deleted' => applicationConstants::YES 
		), array (
				'smt' => static::DB_TBL_PREFIX . 'id = ?',
				'vals' => array (
						$this->mainTableRecordId 
				) 
		) )) {
			$this->error = $db->getError();
			return false;
		}
		
		return true;
	}
}